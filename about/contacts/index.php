<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты | Компания Юг-Авто - официальный диллер Peugeot в Краснодаре");
?>
<div class="container">
<div class="row">

<div class="col-sm-12 col-md-12 pt-3 pb-3">

<h1>Контакты</h1>
<p>Официальный дилер ООО&nbsp;АК&nbsp;«Юг-Авто»</p>
<p>
	 385100, Российская Федерация, Республика Адыгея, Тахтамукайский район, а. Тахтамукай, ул. Краснодарская ,1<br>
</p>
<p>
	 Время работы:
</p>
<p>
	 отдел сервиса с 8.00 до 19.00<br>
	 отдел продаж с 8.00 до 19.00<br>
</p>
<p class="peugeot_call_phone_2">
	 Тел. <a href="tel:+7 (861) 263-00-00">+7 (861) 263-00-00</a>
</p>
<p>
	 E-mail:&nbsp;<a href="mailto:info@peugeot.yug-avto.ru">info@peugeot.yug-avto.ru</a>
</p>
</div>
</div>
</div>
<div><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Aeeef696289f8aa66519815e2e10d917cf28043228e7c195cf1dc9ac122c8e87c&amp;width=100%&amp;height=500&amp;lang=ru_RU&amp;scroll=false"></script></div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>