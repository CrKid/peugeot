<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Связаться с нами");
?>

<div class="container py-5">
  <div class="row">
    <div class="col-md-12"><h1>Связаться с нами</h1></div>
    <div class="col-md-12">
      <?php
            
            $APPLICATION->IncludeComponent(
                "bitrix:form.result.new", 
                "form.inline", 
                array(
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "A",
                    "CHAIN_ITEM_LINK" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "EDIT_URL" => "result_edit.php",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                    "LIST_URL" => "result_list.php",
                    "SEF_MODE" => "N",
                    "SUCCESS_URL" => "",
                    "USE_EXTENDED_ERRORS" => "N",
                    "WEB_FORM_ID" => 4,
                    "COMPONENT_TEMPLATE" => "forms.modal",
                    "VARIABLE_ALIASES" => array(
                        "WEB_FORM_ID" => "WEB_FORM_ID",
                        "RESULT_ID" => "RESULT_ID",
                    )
                ),
                false
            );
        ?>
    </div>
  </div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>