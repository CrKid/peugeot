<?php
	
	require_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/main/include/prolog_before.php");
	
	if ( $_POST ) {
		
		CModule::IncludeModule('iblock');
		CModule::IncludeModule('form');
		
		require $_SERVER['DOCUMENT_ROOT'].'/bitrix/templates/main/include/PHPMailer.php';
		$mailer = new PHPMailer;
		
		$mailer->CharSet = 'utf-8';
		$mailer->setFrom('form_sender@peugeot.yug-avto.ru', 'Сайт Peugeot');
		
		$arForm = CForm::GetByID($_POST['WEB_FORM_ID'])->Fetch();
		$rsQs = CFormField::GetList($arForm['ID'], 'N', $by = 's_id',  $order = 'ASC');
		while ($arQ = $rsQs->Fetch()) {
			$arForm['QS'][$arQ['SID']] = $arQ;
		}
		
		foreach ($arForm['QS'] as $q) {
			
			$ins_name = 'form_text_'.$q['ID'];
			
			if ($q['SID'] == 'NAME') $arIns[$ins_name] = $_POST['NAME'];
			if ($q['SID'] == 'PHONE') $arIns[$ins_name] = $_POST['PHONE'];
			if ($q['SID'] == 'EMAIL') $arIns[$ins_name] = $_POST['EMAIL'];
			if ($q['SID'] == 'RUN') $arIns[$ins_name] = $_POST['RUN'];
			if ($q['SID'] == 'YEAR') $arIns[$ins_name] = $_POST['YEAR'];
			if ($q['SID'] == 'DATE') $arIns[$ins_name] = $_POST['DATE'];
			if ($q['SID'] == 'ADDITIONALLY') $arIns[$ins_name] = $_POST['ADDITIONALLY'];
			if ($q['SID'] == 'COMPANY') $arIns[$ins_name] = $_POST['COMPANY'];
			if ($q['SID'] == 'SOURCE') $arIns[$ins_name] = $_POST['SOURCE'];
			if ($q['SID'] == 'MODEL') $arIns[$ins_name] = $_POST['MODEL'];
			if ( $q['SID'] == 'RECIPIENTS' ) $arIns[$ins_name] = $recs = CIBlockElement::GetProperty(6, 24, [], ['CODE'=>$arForm['SID']])->Fetch()['VALUE']['TEXT'];
			
			//if ( $q['SID'] == 'RECIPIENTS' ) $arIns[$ins_name] = $recs = 'anton.boreckiy@yug-avto.ru';
		}
		
		$res['ins'] = $arIns;
		$res['f'] = $arForm;
		$res['r'] = $recs;
		
		if ( $RESULT_ID = CFormResult::Add($arForm['ID'], $arIns, $check_rights = "N") ) {
			
			$mailer->ClearAddresses();
			foreach ( YApp::findEmails($recs) as $rec ) $mailer->addAddress($rec, '');
			
			$m = '<strong>Посетитель</strong><br />-----------------------------------------------------------------<br />';
			$m .= 'Имя: '.$_POST['NAME'].'<br />'; 
			$m .= 'Телефон: '.$_POST['PHONE'].'<br />'; 
			if ( $_POST['EMAIL'] ) $m .= 'Email: '.$_POST['EMAIL'].'<br />';
			if ( $_POST['COMPANY'] ) $m .= 'Компания: '.$_POST['COMPANY'].'<br />';
			
			if ( $_POST['MODEL'] || $_POST['YEAR'] || $_POST['RUN'] ) {
				
				$m .= '<br /><br />';
				$m .= '<strong>Автомобиль</strong><br />-----------------------------------------------------------------<br />';
				if ( $_POST['MODEL'] ) $m .= 'Автомобиль: '.$_POST['MODEL'].'<br />';
				if ( $_POST['YEAR'] ) $m .= 'Год выпуска: '.$_POST['YEAR'].'<br />';
				if ( $_POST['RUN'] ) $m .= 'Пробег: '.$_POST['RUN'].'<br />';
			}
			
			if ( $_POST['DATE'] ) {
				
				$m .= '<br /><br />';
				$m .= '<strong>Дата</strong><br />-----------------------------------------------------------------<br />';
				if ( $_POST['DATE'] ) $m .= 'Дата: '.$_POST['DATE'].'<br />';
			}
			
			if ( $_POST['ADDITIONALLY'] ) {
				
				$m .= '<br /><br />';
				$m .= '<strong>Сообщение</strong><br />-----------------------------------------------------------------<br />';
				if ( $_POST['ADDITIONALLY'] ) $m .= $_POST['ADDITIONALLY'].'<br />';
			}
			
			if ( $_POST['SOURCE'] ) {
				
				$m .= '<br /><br />';
				$m .= '<strong>Источник</strong><br />-----------------------------------------------------------------<br />';
				if ( $_POST['SOURCE'] ) $m .= $_POST['SOURCE'].'<br />';
			}
			
			$mailer->Subject = 'Заполнена форма '.$arForm['NAME'];
			
			$mailer->msgHTML($m);
			$res['status'] = ( $mailer->Send() ) ? 'success' : 'error';
		}
	}
	
	
	echo json_encode($res);