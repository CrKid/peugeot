<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if( empty($arResult) ) return "";

$strReturn = '<div class="row py-2 breadcrumbs pc"><div class="col-md-12">';
foreach ( $arResult as $k => $arItem ) {
	
	if ( $k < count($arResult)-1 ) $strReturn .= '<a href="'.$arItem['LINK'].'">';
	$strReturn .= $arItem['TITLE'];
	if ( $k < count($arResult)-1 ) $strReturn .= '</a>';
	if ( $k < count($arResult)-1 ) $strReturn .= '<svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>';
}
$strReturn .= '</div></div>';
return $strReturn;
