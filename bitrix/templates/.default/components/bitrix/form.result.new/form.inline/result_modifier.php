<?php
	
		
	$o = ['IBLOCK_SECTION_ID'=>'ASC', 'SORT'=>'ASC'];
	$s = ['ID', 'IBLOCK_ID', 'NAME', 'CODE', 'IBLOCK_SECTION_ID', 'PROPERTY_MENU_TITLE'];
	$f = ['IBLOCK_ID'=>10, 'SECTION_ID'=>[11,12], 'ACTIVE'=>'Y'];
	if ( $arResult['WEB_FORM_NAME'] == 'FORM_TEST_DRIVE' ) $f['PROPERTY_TESTDRIVE_VALUE'] = 'ON';
	
	$res = CIBlockElement::GetList($o, $f, false, false, $s);
	while ( $ob = $res->GetNextElement() ) $arResult['MODELS'][] = $ob->GetFields();