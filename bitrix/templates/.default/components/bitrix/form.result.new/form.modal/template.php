<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die(); ?>

<?php if ($arResult["isFormErrors"] == "Y") echo $arResult["FORM_ERRORS_TEXT"]; ?>

<?php // YApp::sp( $arResult ); ?>
<h2 class="text-uppercase font-weight-light c-p-blue"><?=$arResult['FORM_TITLE']?></h2>

<form class="text-left" name="<?=$arResult['WEB_FORM_NAME']?>" method="POST" enctype="multipart/form-data" data-event="<?=$arResult['arForm']['DESCRIPTION']?>">
  <input type="hidden" name="sessid" id="sessid" value="<?=bitrix_sessid()?>" />
  <input type="hidden" name="WEB_FORM_ID" value="<?=$arResult['arForm']['ID']?>" />
  <fieldset>
	
    <?php foreach ($arResult['QUESTIONS'] as $keyQ => $arQuestion) {
		
		if ($keyQ == 'HIDDEN') {
			?>
            <input type="hidden" role="HIDDEN" name="<?=$keyQ?>" value="" />
            <?php
		} else {
			
			?>
            <div class="form-group">
            <?php
			
			$d = explode('_', $keyQ)[0];
			
			switch ( $d ) {
				
				case 'DELIMITER' :
					?>
                	<legend><?=$arQuestion['CAPTION']?></legend>
                	<?php
				 	break;
					
				case 'MODEL':
					?>
                    <select 
                    	class="form-control" 
                        role="model" 
                        name="<?=$keyQ?>" 
						<?=(($arQuestion['REQUIRED']=='Y')?'required':'')?>
                    >
                    	<option <?=((!$_GET['model'])?'selected':'')?> disabled>Выберите автомобиль<?=(($arQuestion['REQUIRED']=='Y')?' *':'')?></option>
                    	<?php
							$sections[] = (int)$$arResult['MODELS'][0]['IBLOCK_SECTION_ID'];
							foreach ( $arResult['MODELS'] as $keyM => $model ) { 
								
								if ( !in_array((int)$model['IBLOCK_SECTION_ID'], $sections) ) {
									
									$sections[] = (int)$model['IBLOCK_SECTION_ID'];
									?>
                                    </optgroup>
                					<optgroup label="<?=CIBlockSection::GetByID((int)$model['IBLOCK_SECTION_ID'])->GetNextElement()->GetFields()['NAME']?>">
									<?php
								}
								?>
								<option value="<?=$model['CODE']?>" <?=(($model['CODE']==$_GET['model'])?'selected':'')?>><?=$model['PROPERTY_MENU_TITLE_VALUE']?></option>
								<?php
							} // foreach Models
							?>
                            </optgroup>
                            <?php if ( $arResult['WEB_FORM_NAME'] != 'FORM_TEST_DRIVE' ) { ?><option value="other">Другая</option><?php } ?>
                    </select>
                    <?php
					break;
					
				case 'ADDITIONALLY':
					?>
                    <textarea 
                    	class="form-control" 
                        data="<?=$keyQ?>" 
                        rows="5" 
                        name="<?=$keyQ?>" 
                        placeholder="<?=$arQuestion['CAPTION']?><?=(($arQuestion['REQUIRED']=='Y')?' *':'')?>" 
                        <?=(($arQuestion['REQUIRED']=='Y')?'required':'')?>
                    ></textarea>
                    <?php
					break;
					
				case 'SOURCE':
					break;
				case 'RECIPIENTS':
					break;
				case 'DC':
					break;
				
				case 'PHONE':
					?>
                    <input 
                    	type="phone" 
                        data="<?=$keyQ?>" 
                        class="form-control" 
                        name="<?=$keyQ?>" 
                        placeholder="<?=$arQuestion['CAPTION']?><?=(($arQuestion['REQUIRED']=='Y')?' *':'')?>" 
						<?=(($arQuestion['REQUIRED']=='Y')?'required':'')?>
                    >
                    <?php
					break;
					
				case 'YEAR':
					?>
                    <input 
                    	type="number" 
                        data="<?=$keyQ?>" 
                        class="form-control" 
                        name="<?=$keyQ?>" 
                        placeholder="<?=$arQuestion['CAPTION']?><?=(($arQuestion['REQUIRED']=='Y')?' *':'')?>" 
                        min="1960" max="<?=date('Y')?>" step="1" 
						<?=(($arQuestion['REQUIRED']=='Y')?'required':'')?>
                    >
                    <?php
					break;
					
				case 'RUN':
					?>
                    <input 
                    	type="number" 
                        data="<?=$keyQ?>" 
                        class="form-control" 
                        name="<?=$keyQ?>" 
                        placeholder="<?=$arQuestion['CAPTION']?><?=(($arQuestion['REQUIRED']=='Y')?' *':'')?>" 
                        min="0" step="1" 
						<?=(($arQuestion['REQUIRED']=='Y')?'required':'')?>
                    >
                    <?php
					break;
					
				default:
					?>
                    <input 
                    	type="text" 
                        data="<?=$keyQ?>" 
                        class="form-control" 
                        name="<?=$keyQ?>" 
                        placeholder="<?=$arQuestion['CAPTION']?><?=(($arQuestion['REQUIRED']=='Y')?' *':'')?>" 
						<?=(($arQuestion['REQUIRED']=='Y')?'required':'')?>
                    >
                    <?php
					break;
				 
			 } // switch
			 
			 
			 ?>
             </div>
             <?php
		}
		
	} // foreach QUESTIONS ?>
    <div class="form-group my-4"><a href="#" class="but but-blue" role="SendForm"><?=$arResult['arForm']['BUTTON']?></a></div>
    <div class="form-group">
      <div class="rules checked my-2" data-action="personal">
        <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-checked"></use></svg> 
        При отправке заявки вы соглашаетесь на <a href="/about/personal/" target="_blank">использование ваших персональных данных</a>
      </div>
      <div class="rules checked my-2" data-action="communication">
        <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-checked"></use></svg> 
        При отправке заявки вы соглашаетесь на <a href="/about/personal/" target="_blank">рекламную коммуникацию</a>
      </div>
    </div>
    <div class="form-group">
      <p><small>Поля, помеченные * - обязательны для заполнения</small></p>
    </div>
  </fieldset>
</form>
<div class="alert alert-dismissible alert-danger bg-yaerror border-0">К сожалению, что-то пошло не так. Повторите попытку позднее.</div>
<div class="alert alert-dismissible alert-success bg-yasuccess border-0">Спасибо за Вашу заявку! Мы свяжемся с Вами в ближайшее время.</div>