<?php
	
	$tmp['MENU'] = $arResult;
	
	// Пассажирские
	$o = ['SORT'=>'ASC'];
	$f = ['IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'IBLOCK_SECTION_ID'=>11];
	$s = ['ID', 'IBLOCK_ID', 'SECTION_ID', 'DETAIL_PAGE_URL', 'CODE', 'PROPERTY_MENU_TITLE'];
	
	$rs = CIBlockElement::GetList( $o, $f, false, false, $s );
	while( $ob = $rs->GetNextElement() ) {
		
		$s = $ob->GetFields();
		$tmp['ITEMS'][] = [
			'TEXT' => $s['PROPERTY_MENU_TITLE_VALUE'],
			'LINK' => $s['DETAIL_PAGE_URL']
		];
	}
	
	$arResult = $tmp;