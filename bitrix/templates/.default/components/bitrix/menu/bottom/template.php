<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="col-md-4">
  <h5 class="font-weight-normal text-uppercase">Меню</h5>
  <hr />
  <ul class="list-unstyled text-uppercase">
    <?php foreach ( $arResult['MENU'] as $arItem ) { ?>
    <li class="pb-2"><a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
    <?php } // foreach ?>
  </ul>
</div>
<div class="col-md-4">
  <h5 class="font-weight-normal text-uppercase">Модельный ряд</h5>
  <hr />
  <ul class="list-unstyled text-uppercase multicolumn">
    <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
    <li class="pb-2"><a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
    <?php } // foreach ?>
  </ul>
</div>

