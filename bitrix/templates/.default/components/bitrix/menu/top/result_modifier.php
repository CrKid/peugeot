<?php
	
	
	foreach ( $arResult as $k => $arItem ) {
		
		if ( $arItem['PARAMS']['section'] == 'models' && !$arItem['PARAMS']['include'] ) {
			
			// Пассажирские
			$sub = [];
			$sec = CIBlockSection::GetByID(11)->GetNext();
			$sub['NAME'] = $sec['NAME'];
			$o = ['SORT'=>'ASC'];
			$f = ['IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'IBLOCK_SECTION_ID'=>11];
			$s = ['ID', 'IBLOCK_ID', 'SECTION_ID', 'CODE', 'PROPERTY_MENU_TITLE'];
			
			$rs = CIBlockElement::GetList( $o, $f, false, false, $s );
			while( $ob = $rs->GetNextElement() ) {
				
				$s = $ob->GetFields();
				$sub['ITEMS'][] = [
					'TEXT' => $s['PROPERTY_MENU_TITLE_VALUE'],
					'LINK' => '/'.$arItem['PARAMS']['section'].'/'.$sec['CODE'].'/'.$s['CODE'].'/'
				];
			}
			
			$arResult[$k]['SUB'][] = $sub;
			
			// Коммерческие
			$sub = [];
			$sec = CIBlockSection::GetByID(12)->GetNext();
			$sub['NAME'] = $sec['NAME'];
			$o = ['SORT'=>'ASC'];
			$f = ['IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'IBLOCK_SECTION_ID'=>12];
			$s = ['ID', 'IBLOCK_ID', 'SECTION_ID', 'CODE', 'PROPERTY_MENU_TITLE'];
			
			$rs = CIBlockElement::GetList( $o, $f, false, false, $s );
			while( $ob = $rs->GetNextElement() ) {
				
				$s = $ob->GetFields();
				$sub['ITEMS'][] = [
					'TEXT' => $s['PROPERTY_MENU_TITLE_VALUE'],
					'LINK' => '/'.$arItem['PARAMS']['section'].'/'.$sec['CODE'].'/'.$s['CODE'].'/'
				];
			}
			
			$arResult[$k]['SUB'][] = $sub;
			
			// Переоборудованные
			/*
			$sub = [];
			$sub['NAME'] = CIBlockSection::GetByID(13)->GetNext()['NAME'];
			$f = ['IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'SECTION_ID'=>13];
			$s = ['ID', 'IBLOCK_ID', 'NAME', 'CODE'];
			
			$rs = CIBlockSection::GetList( $o, $f, false, $s, false );
			while( $ob = $rs->GetNext() ) {
				
				$sub['ITEMS'][] = [
					'TEXT' => $ob['NAME'],
					'LINK' => '/'.$arItem['PARAMS']['section'].'/'.$sec['CODE'].'/'.$ob['CODE'].'/'
				];
			}
			
			$arResult[$k]['SUB'][] = $sub;
			*/
			
			// Коммерческие
			$sub = [];
			$sec = CIBlockSection::GetByID(35)->GetNext();
			$sub['NAME'] = $sec['NAME'];
			$o = ['SORT'=>'ASC'];
			$f = ['IBLOCK_ID'=>10, 'ACTIVE'=>'Y', 'IBLOCK_SECTION_ID'=>35];
			$s = ['ID', 'IBLOCK_ID', 'SECTION_ID', 'CODE', 'PROPERTY_MENU_TITLE'];
			
			$rs = CIBlockElement::GetList( $o, $f, false, false, $s );
			while( $ob = $rs->GetNextElement() ) {
				
				$s = $ob->GetFields();
				$sub['ITEMS'][] = [
					'TEXT' => $s['PROPERTY_MENU_TITLE_VALUE'],
					'LINK' => '/'.$arItem['PARAMS']['section'].'/'.$sec['CODE'].'/'.$s['CODE'].'/'
				];
			}
			
			$arResult[$k]['SUB'][] = $sub;
			
			// Покупателям
			$sub = [];
			$sub['NAME'] = 'Покупателям';
			include $_SERVER['DOCUMENT_ROOT'].'/'.$arItem['PARAMS']['section'].'/.menu_items.menu.php';
			foreach ( $aMenuLinks as $a ) {
				
				$sub['ITEMS'][] = [
					'TEXT' => $a[0],
					'LINK' => $a[1]
				];
			}
			
			$arResult[$k]['SUB'][] = $sub;
			
			// Кнопки
			include $_SERVER['DOCUMENT_ROOT'].'/'.$arItem['PARAMS']['section'].'/.buttons.menu.php';
			$arResult[$k]['BUTTONS'] = $aMenuLinks;
		
		} elseif ( $arItem['PARAMS']['include'] == 'true' ) {
			
			include $_SERVER['DOCUMENT_ROOT'].'/'.$arItem['PARAMS']['section'].'/.menu_items.menu.php';
			foreach ( $aMenuLinks as $a ) {
				
				$sub = [];
				$sub['NAME'] = $a[0];
				if ( $a[3]['link'] == 'true' ) $sub['LINK'] = $a[1];
				if ( $a[3]['include'] ) {
					
					include $_SERVER['DOCUMENT_ROOT'].'/'.$arItem['PARAMS']['section'].'/'.$a[3]['include'].'/.menu_items.menu.php';
					foreach ( $aMenuLinks as $a ) {
				
						$sub['ITEMS'][] = [
							'TEXT' => $a[0],
							'LINK' => $a[1]
						];
					}
				
				} elseif ( $a[3]['iblock'] ) {
					
					$f = ['IBLOCK_ID'=>$a[3]['iblock'], 'ACTIVE'=>'Y', 'ACTIVE_DATE'=>'Y'];
					$s = ['ID', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'CODE', 'NAME'];
					$rs = CIBlockElement::GetList( $o, $f, false, false, $s );
					while( $ob = $rs->GetNextElement() ) {
						
						$s = $ob->GetFields();
						$sub['ITEMS'][] = [
							'TEXT' => $s['NAME'],
							'LINK' => $s['DETAIL_PAGE_URL']
						];
					}
				}
				
				$arResult[$k]['SUB'][] = $sub;
				
				if ( file_exists($_SERVER['DOCUMENT_ROOT'].'/'.$arItem['PARAMS']['section'].'/.buttons.menu.php') ) {
					
					include $_SERVER['DOCUMENT_ROOT'].'/'.$arItem['PARAMS']['section'].'/.buttons.menu.php';
					$arResult[$k]['BUTTONS'] = $aMenuLinks;
				}
			}
		}
	}

