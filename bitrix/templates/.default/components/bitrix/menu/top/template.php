<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<nav class="bg-p-ddarkblue c-yawhite">
  <div class="container menu">
    <div class="row pc">
      <div class="col-md-2 pt-4 pb-3"><a href="/"><img class="mb-2" src="<?=YApp::MT()?>/assets/images/peugeot-logo.png" /></a></div>
      <div class="col-md-8 d-flex flex-wrap align-content-end justify-content-center">
        <ul class="list-inline m-0">
          <?php foreach ( $arResult as $k => $arItem ) { ?>
          <li class="list-inline-item pb-3 <?=(($k<count($arResult)-1)?'mr-4':'')?>" role="showNav" data-section="<?=$arItem['PARAMS']['section']?>"><a href="<?=$arItem['LINK']?>"><?=$arItem['TEXT']?></a></li>
          <?php } // foreach ?>
        </ul>
      </div>
      <div class="col-md-2 d-flex flex-wrap align-content-end justify-content-end">
        <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA-M"></use>
        </svg>
        <div class="address"><a href="#"><?=$GLOBALS['SETTINGS']['ADDRESS']?></a></div>
        <div class="phone peugeot_call_phone_2"><a href="tel:+<?=YApp::phoneIn($GLOBALS['SETTINGS']['PHONE'])?>"><?=YApp::phoneOut($GLOBALS['SETTINGS']['PHONE'])?></a></div>
      </div>
    </div>
    <?php if ( $APPLICATION->GetCurPage(false) !== '/' ) $APPLICATION->IncludeComponent('bitrix:breadcrumb', 'top', [], false) ?>
    <div class="row mob">
      <div class="col pt-4 pb-3"><a href="/"><img class="mb-2" src="<?=YApp::MT()?>/assets/images/peugeot-logo.png" /></a></div>
      <div class="col pt-4 text-right" role="showNavM">
        <svg xmlns="http://www.w3.org/2000/svg">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-burger"></use>
        </svg>
      </div>
    </div>
  </div>
</nav>

<?php foreach ( $arResult as $k => $arItem ) { ?>

<div class="nav-section bg-p-lightwhite w-100" data-section="<?=$arItem['PARAMS']['section']?>">
  <div class="container">
    <div class="row py-5">
      <div class="col-md-12 mb-3"><div class="h2"><?=$arItem['TEXT']?></div></div>
      <?php $cols_count = count($arItem['SUB']); ?>
      <?php foreach ( $arItem['SUB'] as $arSub ) { ?>
      <div class="col">
        <div class="nav-section-title mb-3 position-relative <?=(($arSub['LINK'])?'link':'')?>">
		  <?php if ( $arSub['LINK'] ) { ?><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg> <a href="<?=$arSub['LINK']?>"><?php } ?>
		  <?=$arSub['NAME']?>
          <?php if ( $arSub['LINK'] ) { ?></a><?php } ?>
        </div>
        <ul class="list-unstyled">
          <?php foreach ( $arSub['ITEMS'] as $item ) { ?>
          <li class="pb-2 pl-3 position-relative">
            <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg> 
            <a href="<?=$item['LINK']?>"><?=$item['TEXT']?></a>
          </li>
          <?php } // foreach ?>
        </ul>
      </div>
      <?php } // foreach ?>
      <?php for ( $i=1; $i<=4-$cols_count; $i++ ) { ?>
      <div class="col"></div>
      <?php } // for ?>
      <div class="col-2">
        <?php foreach ( $arItem['BUTTONS'] as $item ) { ?>
        <a class="but but-blue w-100 d-block pt-1 mb-2" href="<?=$item[1]?>"><?=$item[0]?></a>
        <?php } // foreach ?>
      </div>
    </div>
  </div>
  <a href="#" class="close-section p-2"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-close"></use></svg></a>
</div>
<?php } // foreach ?>
<div class="nav-cover"></div>


<!-- Mobile menu -->
<div class="container nav-section-m bg-p-lightwhite position-absolute w-100 mob">
  
  <div class="row main-level">
    <?php foreach ( $arResult as $kR => $arItem ) { ?>
      <div class="col-md-12 py-3 pr-5 position-relative" role="show-sub" data-level="1" data-upsect="<?=$arItem['PARAMS']['section']?>" data-sectm="<?=$kR?>">
        <div class="h5 p-0 m-0 font-weight-light text-uppercase"><?=$arItem['TEXT']?></div>
        <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
      </div>
	<?php } // foreach ?>
  </div>
  
  <?php foreach ( $arResult as $kR => $arItem ) { ?>
    <div class="row sub-level bg-p-lightwhite position-absolute w-100 h-100" data-level="1" data-upsect="<?=$arItem['PARAMS']['section']?>" data-sectm="<?=$kR?>">
      <div class="col-md-12 px-0 py-4 sub-title c-yawhite position-relative bg-p-darkwhite" role="show-up" data-level="0">
        <div class="h5 m-0 text-center font-weight-light text-uppercase"><?=$arItem['TEXT']?></div>
        <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use></svg>
      </div>
      <?php foreach ( $arItem['SUB'] as $kS => $arSub ) { ?>
      <div class="col-md-12 py-3 pr-5 sub-item position-relative" role="show-sub" data-level="2" data-upsect="<?=$arItem['PARAMS']['section']?>" data-sectm="<?=$kS?>">
        <div class="h5 m-0 font-weight-light text-uppercase">
		  <?php if ( $arSub['LINK'] ) { ?><a href="<?=$arSub['LINK']?>"><?php } ?>
		  <?=$arSub['NAME']?>
          <?php if ( $arSub['LINK'] ) { ?></a><?php } ?>
        </div>
        <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
      </div>
      <?php } // foreach ?>
    </div>
    <?php foreach ( $arItem['SUB'] as $kS => $arSub ) { ?>
	  <?php if ( $arSub['ITEMS'] ) { ?>
        <div class="row sub-level bg-p-lightwhite position-absolute w-100 h-100" data-level="2" data-upsect="<?=$arItem['PARAMS']['section']?>" data-sectm="<?=$kS?>">
          <div class="col-md-12 px-0 py-4 sub-title c-yawhite position-relative bg-p-darkwhite" role="show-up">
            <div class="h5 m-0 text-center font-weight-light text-uppercase"><?=$arSub['NAME']?></div>
            <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use></svg>
          </div>
          <?php foreach ( $arSub['ITEMS'] as $item ) { ?>
          <div class="col-md-12 py-3 pr-5 sub-item position-relative">
            <a href="<?=$item['LINK']?>" class="h5 m-0 font-weight-light text-uppercase"><?=$item['TEXT']?></a>
            <svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use></svg>
          </div>
          <?php } // foreach ?>
        </div>
      <?php } // if ?>
    <?php } // foreach ?>
    
  <?php } // foreach ?>
  
</div>




