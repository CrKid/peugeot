<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="bg-p-llightwhite py-5">
  <div class="container">
    <div class="row mb-3">
      <div class="col-md-12 text-center"><div class="h2 font-weight-normal text-uppercase">О дилере</div></div>
      <?php // YApp::sp( $arResult ); ?>
    </div>
    <div class="row main-dealer">
      <div class="col-md-2 mb-3">
        <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#Logo-YA-M"></use>
        </svg>
      </div>
      <div class="col-md-2 pt-2">
        <h5 class="text-uppercase mb-3">Контакты</h5>
        <div class="phone peugeot_call_phone_2"><a href="tel:+<?=YApp::phoneIn($arResult['PROPERTIES']['PHONE']['VALUE'])?>"><?=YApp::phoneOut($arResult['PROPERTIES']['PHONE']['VALUE'])?></a></div>
        <a href="mailto:<?=$arResult['PROPERTIES']['EMAIL']['VALUE']?>"><?=$arResult['PROPERTIES']['EMAIL']['VALUE']?></a>
      </div>
      <div class="col-md-2 pt-2">
        <h5 class="text-uppercase mb-3">Адрес</h5>
        <p><?=$arResult['PROPERTIES']['ADDRESS']['VALUE']?></p>
      </div>
      <div class="col-md-3 pt-2">
        <h5 class="text-uppercase mb-3">Режим работы</h5>
        <p>
		  Отдел продаж: <?=$arResult['PROPERTIES']['SCHEDULE_SALE']['VALUE']?><br />
          Отдел сервиса: <?=$arResult['PROPERTIES']['SCHEDULE_SERVICE']['VALUE']?>
        </p>
      </div>
      <div class="col-md-3 pt-2">
        <h5 class="text-uppercase mb-3">О нас</h5>
        <p><?=$arResult['PREVIEW_TEXT']?></p>
      </div>
    </div>
    <div class="row main-dealer mt-5">
      <div class="col-md-12"><h5 class="text-uppercase mb-3">Как нас найти</h5></div>
      <div class="col-md-12">
        <?=$arResult['PROPERTIES']['MAP']['~VALUE']['TEXT']?>
      </div>
    </div>
  </div>
</div>