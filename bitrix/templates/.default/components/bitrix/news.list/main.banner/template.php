<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
  <div class="row position-relative">
    <?php
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="col p-0">
      <div class="swiper-container swiper-banner">
        <div class="swiper-wrapper">
          <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
          <div class="swiper-slide"  data-link="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>">
            <img class="w-100 pc" src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" />
            <img class="w-100 mob" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
            <a class="but but-blue banner-a pc" href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" target="<?=(($arItem['PROPERTIES']['BLANK']['VALUE'])?'_blank':'_self')?>">Подробнее</a>
          </div>
          <?php } // foreach ?>
        </div>
        <!-- Add Arrows -->
        <div class="swiper-arrow swiper-next pc">
          <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
          </svg>
        </div>
        <div class="swiper-arrow swiper-prev pc">
          <svg xmlns="http://www.w3.org/2000/svg">
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
          </svg>
        </div>
        
      </div>
    </div>
    <div class="swiper-pagination mob"></div>
  </div>
  <div class="col-md-12 text-center mt-5 mob">
    <a class="but but-blue" role="banner-mobile" href="<?=$arResult['ITEMS'][0]['PROPERTIES']['LINK']['VALUE']?>" target="<?=(($arResult['ITEMS'][0]['PROPERTIES']['BLANK']['VALUE'])?'_blank':'_self')?>">Подробнее</a>
  </div>
</div>
<?php // swiper -> global
	$GLOBALS['SWIPER'][] = [
		'VAR' => 'JSApp.Swipers.SwBanner',
		'CLASS' => '.swiper-banner',
		'NAV' => [
			'NEXT' => '.swiper-banner .swiper-next',
			'PREV' => '.swiper-banner .swiper-prev'
		],
		'LOOP' => true,
		'PAG' => '.swiper-pagination',
		'AUTO' => true
	];
?>