<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container py-5">
  <div class="row">
    <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
    <?php
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="col-md-3">
      <div class="card border-0">
        <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" target="<?=$arItem['PROPERTIES']['TARGET']['VALUE']?>">
          <img src="<?=$arItem['DETAIL_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>" />
        </a>
        <div class="card-body">
          <p class="card-text py-4 text-center">
            <a href="<?=$arItem['PROPERTIES']['LINK']['VALUE']?>" class="but but-blue"><?=$arItem['NAME']?></a>
          </p>
        </div>
      </div>
    </div>
    <?php } // foreach ?>
  </div>
</div>