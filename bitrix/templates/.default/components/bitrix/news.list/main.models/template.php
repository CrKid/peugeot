<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="container py-5">
  <div class="row mb-3">
    <div class="col-md-12 text-center"><div class="h2 font-weight-normal text-uppercase">Модельный ряд</div></div>
  </div>
  <div class="row position-relative">
    <?php // YApp::sp( $arResult['ITEMS'][0] ); ?>
    <div class="col-1"></div>
    
    <div class="col p-0 swiper-container swiper-mainmodels pc">
      <div class="swiper-wrapper">
		<?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
        <?php
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
        <div class="card swiper-slide">
          <div class="card-body"><h5 class="card-title"><?=$arItem['PROPERTIES']['MENU_TITLE']['VALUE']?></h5></div>
          <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>">
          <div class="card-body cars-main-preview">
            <p class="card-text">
              от
              <br />
              <strong><?=number_format((int)$arItem['PROPERTIES']['PRICE']['VALUE'], 0, '', ' ')?> ₽ <sup>*</sup></strong>
            </p>
            <p class="card-text py-4 text-center"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="but but-blue">Подробнее</a></p>
          </div>
        </div>
        <?php } // foreach ?>
      </div>
    </div>
    
    <div class="col-md-10 p-0 swiper-container swiper-mainmodels-m mob">
      <div class="swiper-wrapper">
		<?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
        <?php
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
        <div class="card swiper-slide">
          <div class="card-body"><h5 class="card-title"><?=$arItem['PROPERTIES']['MENU_TITLE']['VALUE']?></h5></div>
          <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>">
          <div class="card-body">
            <p class="card-text">
              от
              <br />
              <strong><?=number_format((int)$arItem['PROPERTIES']['PRICE']['VALUE'], 0, '', ' ')?> ₽ <sup>*</sup></strong>
            </p>
            <p class="card-text py-4 text-center"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="but but-blue">Подробнее</a></p>
          </div>
        </div>
        <?php } // foreach ?>
      </div>
    </div>
    
    <div class="swiper-arrow swiper-next" role="swiper-mainmodels-next">
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
      </svg>
    </div>
    <div class="swiper-arrow swiper-prev" role="swiper-mainmodels-prev">
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
      </svg>
    </div>
    
    <div class="swiper-arrow swiper-next mob" role="swiper-mainmodels-next-m">
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
      </svg>
    </div>
    <div class="swiper-arrow swiper-prev mob" role="swiper-mainmodels-prev-m">
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
      </svg>
    </div>
    
    <div class="col-1"></div> 
  
  </div>
  <div class="row position-relative">
    <a href="#" class="toggle-disclamer position-absolute bg-p-blue p-1" role="disclamer"><svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-down"></use></svg></a> 
    <div class="col bg-p-blue c-yawhite disclamer showed" role="disclamer">
      <p class="pt-3 px-3 pb-1">* Указанная цена включает все действующие спецпредложения. Может отличаться от цены, установленной дилером. Подробности в разделе «Специальные преложения» в шоуруме каждой модели и в разделе ....</p>
    </div>
  </div>
</div>

<?php // swiper -> global
	$GLOBALS['SWIPER'][] = [
		'VAR' => 'JSApp.Swipers.SwMModels',
		'CLASS' => '.swiper-mainmodels',
		'NAV' => [
			'NEXT' => '.swiper-next[role="swiper-mainmodels-next"]',
			'PREV' => '.swiper-prev[role="swiper-mainmodels-prev"]'
		],
		'LOOP' => true,
		'SLIDES' => 3,
		'SPACE' => 'null'
	];
	
	$GLOBALS['SWIPER'][] = [
		'VAR' => 'JSApp.Swipers.SwMModelsM',
		'CLASS' => '.swiper-mainmodels-m',
		'NAV' => [
			'NEXT' => '.swiper-next[role="swiper-mainmodels-next-m"]',
			'PREV' => '.swiper-prev[role="swiper-mainmodels-prev-m"]'
		],
		'LOOP' => true,
	];
	
	//YApp::sp( $GLOBALS['SWIPER'] );
?>