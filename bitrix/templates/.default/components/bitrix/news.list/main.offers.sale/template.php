<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container py-5">
  <div class="row mb-3">
    <div class="col-md-12 text-center"><div class="h2 font-weight-normal text-uppercase">Специальные предложения</div></div>
  </div>
  <div class="row pc">
    <?php // YApp::sp( $arResult['ITEMS'][0] ); ?>
    <div class="col-1"></div>
    <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
    <?php
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="col p-0 mr-1">
      <div class="card bg-p-llightwhite border-0">
        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>">
        <div class="card-body">
          <h5 class="card-title"><?=$arItem['NAME']?></h5>
          <p class="card-text">
            Максимальная выгода до <?=number_format((int)$arItem['PROPERTIES']['DISCOUNT']['VALUE'], 0, '', ' ')?> ₽
            <br />
            <?php if ( (int)$arItem['PROPERTIES']['CREDIT']['VALUE'] ) { ?>
			<br />
			Кредит за <?=number_format((int)$arItem['PROPERTIES']['CREDIT']['VALUE'], 0, '', ' ')?> ₽
			<?php } ?>
          </p>
          <p class="card-text py-4 text-center"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="but but-blue">Подробнее</a></p>
        </div>
      </div>
    </div>
    <?php } // foreach ?> 
    <div class="col-1"></div>
  </div>
  <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
  <div class="row mb-5 mob">
    <div class="col-1"></div>
    <div class="col">
      <div class="card bg-p-llightwhite border-0">
        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>">
        <div class="card-body">
          <h5 class="card-title"><?=$arItem['NAME']?></h5>
          <p class="card-text">
            Максимальная выгода<br />до <?=number_format((int)$arItem['PROPERTIES']['DISCOUNT']['VALUE'], 0, '', ' ')?> ₽
            <br />
            <?php if ( (int)$arItem['PROPERTIES']['CREDIT']['VALUE'] ) { ?>
			<br />
			Кредит за <?=number_format((int)$arItem['PROPERTIES']['CREDIT']['VALUE'], 0, '', ' ')?> ₽
			<?php } ?>
          </p>
          <p class="card-text py-4 text-center"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="but but-blue">Подробнее</a></p>
        </div>
      </div>
    </div>
    <div class="col-1"></div>
  </div>
  <?php } // foreach ?> 
</div>