<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="container py-5">
  <div class="row mb-3">
    <div class="col-md-12 text-center"><div class="h2 font-weight-normal text-uppercase">Сервисные акции</div></div>
    <?php // YApp::sp( $arResult['ITEMS'] ); ?>
  </div>
  <div class="row">
    <?php // YApp::sp( $arResult['ITEMS'][0] ); ?>
    <div class="col-md-1"></div>
    <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
    <?php
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="col-md-5">
      <div class="card border-0">
        <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>" /></a>
        <div class="card-body">
          <p class="card-text py-4 text-center">
            <a href="<?=(($arItem['PROPERTIES']['USE_FORM']['VALUE_XML_ID'])?'#'.$arItem['PROPERTIES']['USE_FORM']['VALUE_XML_ID']:$arItem['DETAIL_PAGE_URL'])?>" class="but but-blue"><?=(($arItem['PROPERTIES']['USE_FORM']['VALUE'])?:'Подробнее')?></a>
          </p>
        </div>
      </div>
    </div>
    <?php } // foreach ?> 
    <div class="col-md-1"></div>
  </div>
</div>