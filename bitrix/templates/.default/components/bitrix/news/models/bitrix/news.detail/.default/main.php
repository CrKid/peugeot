<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1><?=$arResult['PROPERTIES']['MENU_TITLE']['VALUE']?></h1>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <p><?=$arResult['PREVIEW_TEXT']?></p>
    </div>
  </div>
</div>

<div class="container my-5">
  <div class="row">
    <div class="col-md-12 text-center pc">
      <?php if ($arResult['OFFER']) { ?>
      <a href="<?=$arResult['OFFER']['DETAIL_PAGE_URL']?>" class="but but-blue">Получить спецпредложение</a>
      <?php } ?>
      <?php /* <a href="#" class="but but-blue">Заявка на трейд-ин</a> */ ?>
      <?php if ($arResult['PROPERTIES']['PREORDER']['VALUE']=='ON') { ?>
      <a href="/buyers/order/?model=<?=$arResult['CODE']?>" class="but but-blue">Оформить предзаказ</a>
      <?php } else { ?>
        <?php if ($arResult['PROPERTIES']['CREDIT']['VALUE']=='ON') { ?>
        <a href="/buyers/credit/calculator/" class="but but-blue">Заявка на кредит</a>
        <?php } ?>
      <?php } ?>
      <?php /* if ($arResult['PROPERTIES']['BUSINESS_QUERY']['VALUE']=='ON') { ?>
      <a href="/services/corporate/business/" class="but but-blue">Отправить бизнес-запрос</a>
      <?php } */ ?>
      <?php if ($arResult['PROPERTIES']['TESTDRIVE']['VALUE']=='ON') { ?>
      <a href="/buyers/test-drive/?model=<?=$arResult['CODE']?>" class="but but-blue">Записаться на тест-драйв</a>
      <?php } ?>
      <?php if ($arResult['PROPERTIES']['BOOKLET']['VALUE']) { ?>
      <a href="<?=CFile::GetPath($arResult['PROPERTIES']['BOOKLET']['VALUE'])?>" class="but but-blue">Скачать брошюру</a>
      <?php } ?>
      <?php if ($arResult['PROPERTIES']['PRICELIST']['VALUE']) { ?>
      <a href="<?=CFile::GetPath($arResult['PROPERTIES']['PRICELIST']['VALUE'])?>" class="but but-blue">Скачать прайс-лист</a>
      <?php } ?>
      <?php if ($arResult['PROPERTIES']['ACCESSORIES']['VALUE']) { ?>
      <a href="<?=CFile::GetPath($arResult['PROPERTIES']['ACCESSORIES']['VALUE'])?>" class="but but-blue">Каталог аксессуаров</a>
      <?php } ?>
      <?php if ($arResult['PROPERTIES']['SPECIFICATIONS']['VALUE']) { ?>
      <a href="<?=CFile::GetPath($arResult['PROPERTIES']['SPECIFICATIONS']['VALUE'])?>" class="but but-blue">Технические характеристики</a>
      <?php } ?>
    </div>
    <?php if ($arResult['OFFER']) { ?>
    <div class="col-md-12 py-3 py-3 text-center mob"><a href="<?=$arResult['OFFER']['DETAIL_PAGE_URL']?>" class="but but-blue">Получить спецпредложение</a></div>
    <?php } ?>
    <?php /* <a href="#" class="but but-blue">Заявка на трейд-ин</a> */ ?>
    <?php if ($arResult['PROPERTIES']['PREORDER']['VALUE']=='ON') { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="/buyers/order/?model=<?=$arResult['CODE']?>" class="but but-blue">Оформить предзаказ</a></div>
    <?php } else { ?>
      <?php if ($arResult['PROPERTIES']['CREDIT']['VALUE']=='ON') { ?>
      <div class="col-md-12 py-3 text-center mob"><a href="/buyers/credit/calculator/" class="but but-blue">Заявка на кредит</a></div>
      <?php } ?>
    <?php } ?>
    <?php /* if ($arResult['PROPERTIES']['BUSINESS_QUERY']['VALUE']=='ON') { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="/services/corporate/business/" class="but but-blue">Отправить бизнес-запрос</a></div>
    <?php } */?>
    <?php if ($arResult['PROPERTIES']['TESTDRIVE']['VALUE']=='ON') { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="/buyers/test-drive/?model=<?=$arResult['CODE']?>" class="but but-blue">Записаться на тест-драйв</a></div>
    <?php } ?>
    <?php if ($arResult['PROPERTIES']['BOOKLET']['VALUE']) { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="<?=CFile::GetPath($arResult['PROPERTIES']['BOOKLET']['VALUE'])?>" class="but but-blue">Скачать брошюру</a></div>
    <?php } ?>
    <?php if ($arResult['PROPERTIES']['PRICELIST']['VALUE']) { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="<?=CFile::GetPath($arResult['PROPERTIES']['PRICELIST']['VALUE'])?>" class="but but-blue">Скачать прайс-лист</a></div>
    <?php } ?>
    <?php if ($arResult['PROPERTIES']['ACCESSORIES']['VALUE']) { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="<?=CFile::GetPath($arResult['PROPERTIES']['ACCESSORIES']['VALUE'])?>" class="but but-blue">Каталог аксессуаров</a></div>
    <?php } ?>
    <?php if ($arResult['PROPERTIES']['SPECIFICATIONS']['VALUE']) { ?>
    <div class="col-md-12 py-3 text-center mob"><a href="<?=CFile::GetPath($arResult['PROPERTIES']['SPECIFICATIONS']['VALUE'])?>" class="but but-blue">Технические характеристики</a></div>
    <?php } ?>
  </div>
</div>
    
<?php if ( $arResult['ROW_EXC'] != 0 ) { ?>
    
    <div class="container py-5">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>
            
            <?php if ( $arResult['PROPERTIES']['EXC_TITLE']['VALUE'] ) { ?>
            <?=$arResult['PROPERTIES']['EXC_TITLE']['VALUE']?>
            <?php } else { ?>
            Основные преимущества <?=$arResult['PROPERTIES']['MENU_TITLE']['VALUE']?>
            <?php } ?>
            
          </h2>
        </div>
      </div>
    </div>
    
    <?php foreach ($arResult['ROW_EXC'] as $row) { ?>
    
        <div class="container model-tabs">
          <div class="row">
            <?php foreach ($row as $i) { ?>
              <div class="col-md-<?=$arResult['COL_EXC']?> p-0 position-relative" role="model-tab" data-tab="tab<?=$i?>">
                <span class="title p-2 w-100 c-yawhite position-absolute"><?=$arResult['PROPERTIES']['EXC_0'.$i.'_TITLE']['VALUE']?></span>
                <span class="image"><img class="title-img w-100" src="<?=CFile::GetPath($arResult['PROPERTIES']['EXC_0'.$i.'_IMG']['VALUE'])?>" /></span>
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-plus"></use>
                </svg>
                <span class="cover position-absolute"></span>
              </div>
            <?php } ?>
          </div>
        </div>
        
        <?php foreach ( $row as $i ) { ?>
        <div class="container" role="model-tab-content" data-tab="tab<?=$i?>" data-scrole="tab<?=$i?>">
          <div class="row">
            
              <div class="col-md-12 py-3"><h3 class="text-center"><?=$arResult['PROPERTIES']['EXC_0'.$i.'_TITLE']['VALUE']?></h3></div>
              <?php if ($arResult['PROPERTIES']['EXC_0'.$i.'_PICS']['VALUE']) { ?>
              <div class="col-md-6 pb-3">
                <?php if (count($arResult['PROPERTIES']['EXC_0'.$i.'_PICS']['VALUE']) > 1) { ?>
                <div class="swiper-container swiper-excelenses-<?=$i?>" style="height: auto;">
                  <div class="swiper-wrapper">
                    <?php foreach ($arResult['PROPERTIES']['EXC_0'.$i.'_PICS']['VALUE'] as $excPic) { ?>
                    <div class="swiper-slide">
                      <img class="w-100" src="<?=CFile::GetPath($excPic)?>" />
                    </div>
                    <?php } ?>
                  </div>
                  <!-- Add Arrows -->
                  <div class="swiper-arrow swiper-next">
                    <svg xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
                    </svg>
                  </div>
                  <div class="swiper-arrow swiper-prev">
                    <svg xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
                    </svg>
                  </div>
                </div>
                <?php
					$GLOBALS['SWIPER'][] = [
						'VAR' => 'JSApp.Swipers.ModelExcelenses'.$i,
						'CLASS' => '.swiper-excelenses-'.$i,
						'NAV' => [
							'NEXT' => '[data-tab="tab'.$i.'"] .swiper-next',
							'PREV' => '[data-tab="tab'.$i.'"] .swiper-prev'
						],
						'LOOP' => true
					];
                ?>
                <?php } else { ?>
                <img class="w-100" src="<?=CFile::GetPath($arResult['PROPERTIES']['EXC_0'.$i.'_PICS']['VALUE'][0])?>" />
                <?php } // if gallery ?>
              </div>
              <div class="col-md-6 pb-3">
                <?=$arResult['PROPERTIES']['EXC_0'.$i.'_TEXT']['~VALUE']['TEXT']?>
              </div>
              <?php } else { ?>
              <div class="col-md-12 pb-3"><?=$arResult['PROPERTIES']['EXC_0'.$i.'_TEXT']['~VALUE']['TEXT']?></div>
              <?php } // if pics ?>
            </div>
            
          </div>
        </div>
        <?php } // foreach ?>
    
    <?php } // foreach ?>
    
<?php } // if ?>
    
<?php if ( $arResult['PROPERTIES']['MAIN_TEXT_TITLE']['VALUE'] ) { ?>
<div class="container py-5">
  <div class="row">
	<div class="col-md-12 text-center">
	  <h2><?=$arResult['PROPERTIES']['MAIN_TEXT_TITLE']['VALUE']?></h2>
	</div>
  </div>
</div>
<?php } ?>
    
<?php if ($arResult['PROPERTIES']['GALLERY']['VALUE']) { ?>
<div class="container model-tabs no-p-m">
  <div class="row">
	
	<div class="swiper-gallery" style="overflow: hidden; position: relative;">
	  <div class="swiper-wrapper">
		<?php foreach ($arResult['PROPERTIES']['GALLERY']['VALUE'] as $galPic) { ?>
		<div class="swiper-slide">
		  <img class="title-img" src="<?=CFile::GetPath($galPic)?>" />
		</div>
		<?php } ?>
	  </div>
	  <!-- Add Arrows -->
      <div class="swiper-arrow swiper-next">
        <svg xmlns="http://www.w3.org/2000/svg">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
        </svg>
      </div>
      <div class="swiper-arrow swiper-prev">
        <svg xmlns="http://www.w3.org/2000/svg">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
        </svg>
      </div>
	</div>
  </div>
</div>
<?php
	$GLOBALS['SWIPER'][] = [
		'VAR' => 'JSApp.Swipers.Gallery',
		'CLASS' => '.swiper-gallery',
		'NAV' => [
			'NEXT' => '.swiper-arrow.swiper-next',
			'PREV' => '.swiper-arrow.swiper-prev'
		],
		'LOOP' => true,
	];
?>

<?php } // if ?>
	
<?php if ( $arResult['DETAIL_TEXT'] ) { ?>
<div class="container">
  <div class="row">
   <div class="col-md-12">
     <?=$arResult['DETAIL_TEXT']?>
   </div>
   <?php if ( $arResult['PROPERTIES']['MAIN_DISCLAMER']['VALUE'] ) { ?>
   <div class="col-md-12">
     <hr />
     <small><?=$arResult['PROPERTIES']['MAIN_DISCLAMER']['VALUE']['TEXT']?></small>
   </div>
   <?php } ?>
  </div>
</div>
<?php } ?>