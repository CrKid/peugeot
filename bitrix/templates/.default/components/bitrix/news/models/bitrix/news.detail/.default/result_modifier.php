<?
	// Offers for model
	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL");//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
	$arFilter = Array("IBLOCK_ID"=>2, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 'PROPERTY_MODELS' => $arResult['ID']);
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, Array("nPageSize"=>50), $arSelect);
	
	if ( $ob = $res->GetNextElement() ) $arResult['OFFER'] = $ob->GetFields();
	
	
	// Pages for model
	
	$arSelect = [
		'ID',
		'IBLOCK_ID',
		'NAME',
		'CODE',
		'PREVIEW_TEXT'
	];
	
	for ($i = 1; $i <= 20; $i++) {
		
		$arPPsSelect[] = 'SECTION_0'.$i.'_TITLE';
		$arPPsSelect[] = 'SECTION_0'.$i.'_PICS';
		$arPPsSelect[] = 'SECTION_0'.$i.'_SLIDER';
		$arPPsSelect[] = 'SECTION_0'.$i.'_COLUMN';
		$arPPsSelect[] = 'SECTION_0'.$i.'_TEXT';
		$arPPsSelect[] = 'SECTION_0'.$i.'_FULLWIDTHSLIDER';
	}
	
	$pagesBlock = 9;
	
	$arFilter = Array("IBLOCK_ID"=>9, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", 'SECTION_CODE' => $arResult['CODE']);
	
	$res = CIBlockElement::GetList(array("SORT"=>"ASC"), $arFilter, false, false, $arSelect);
	
	while ($ob = $res->GetNextElement()) {
		
		$view = $ob->GetFields();
		$arResult['PAGES_TABS'][$view['CODE']] = $ob->GetFields();
		
		for ($i = 1; $i <= 20; $i++) {
			
			$propSelect = ['CODE' => 'SECTION_0'.$i.'_TITLE'];
			$prop_res = CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], $propSelect);
			if ($prop_ob = $prop_res->GetNext()) {
				if ($prop_ob['VALUE'] != '')
					$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TITLE'] = $prop_ob['VALUE'];
			}
			
			$propSelect = ['CODE' => 'SECTION_0'.$i.'_PICS'];
			$prop_res = CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], $propSelect);
			while ($prop_ob = $prop_res->GetNext()) {
				if ($arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TITLE'] != '')
					$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_PICS'][] = $prop_ob['VALUE'];
			}
			
			$propSelect = ['CODE' => 'SECTION_0'.$i.'_SLIDER'];
			$prop_res = CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], $propSelect);
			if ($prop_ob = $prop_res->GetNext()) {
				if ($arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TITLE'] != '')
					$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_SLIDER'] = $prop_ob['VALUE_ENUM'];
			}
			
			$propSelect = ['CODE' => 'SECTION_0'.$i.'_FULLWIDTHSLIDER'];
			$prop_res = CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], $propSelect);
			if ($prop_ob = $prop_res->GetNext()) {
				if ($arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TITLE'] != '')
					$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_FULLWIDTHSLIDER'] = $prop_ob['VALUE_ENUM'];
			}
			
			$propSelect = ['CODE' => 'SECTION_0'.$i.'_COLUMN'];
			$prop_res = CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], $propSelect);
			if ($prop_ob = $prop_res->GetNext()) {
				if ($arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TITLE'] != '')
					$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_COLUMN'] = $prop_ob['VALUE_ENUM'];
			}
			
			$propSelect = ['CODE' => 'SECTION_0'.$i.'_TEXT'];
			$prop_res = CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], $propSelect);
			if ($prop_ob = $prop_res->GetNext()) {
				if ($arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TITLE'] != '')
					$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SECTION_0'.$i.'_TEXT'] = $prop_ob['~VALUE']['TEXT'];
			}
			
		} // for
		
		$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SEO_TITLE'] = ( CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], ['CODE'=>'SEO_TITLE'])->GetNext()['VALUE'] ) ?: false;
		$arResult['PAGES_TABS'][$view['CODE']]['PROPS']['SEO_DESCRIPTION'] = ( CIBlockElement::GetProperty($pagesBlock, $arResult['PAGES_TABS'][$view['CODE']]['ID'], ['sort' => 'asc'], ['CODE'=>'SEO_DESCRIPTION'])->GetNext()['VALUE'] ) ?: false;
		
	}
	
	$arResult['COUNT_EXC'] = 0;
	for ($i = 1; $i <= 6; $i++) {
		if ($arResult['PROPERTIES']['EXC_0'.$i.'_TITLE']['VALUE']) $arResult['COUNT_EXC']++;
	}
	
	switch ($arResult['COUNT_EXC']) {
		
		case 0:
			$arResult['ROW_EXC'] = 0;
			break;
		
		case 1:
			$_SESSION['COL_EXC'] = $arResult['COL_EXC'] = 12;
			$arResult['ROW_EXC'][] = [1];
			break;
			
		case 2:
			$arResult['COL_EXC'] = 6;
			$arResult['ROW_EXC'][] = [1, 2];
			break;
			
		case 3:
			$arResult['COL_EXC'] = 4;
			$arResult['ROW_EXC'][] = [1, 2, 3];
			break;
		
		case 4:
			$arResult['COL_EXC'] = 6;
			$arResult['ROW_EXC'][] = [1, 2];
			$arResult['ROW_EXC'][] = [3, 4];
			break;
			
		case 5:
			$arResult['COL_EXC'] = 4;
			$arResult['ROW_EXC'][] = [1, 2, 3];
			$arResult['ROW_EXC'][] = [4, 5];
			break;
		
		case 6:
			$arResult['COL_EXC'] = 4;
			$arResult['ROW_EXC'][] = [1, 2, 3];
			$arResult['ROW_EXC'][] = [4, 5, 6];
			$arResult['ROW_EXC'][] = [7];
			break;
		
		case 7:
			$arResult['COL_EXC'] = 4;
			$arResult['ROW_EXC'][] = [1, 2, 3];
			$arResult['ROW_EXC'][] = [4, 5, 6];
			$arResult['ROW_EXC'][] = [7, 8];
			break;
			
		case 8:
			$arResult['COL_EXC'] = 4;
			$arResult['ROW_EXC'][] = [1, 2, 3];
			$arResult['ROW_EXC'][] = [4, 5, 6];
			$arResult['ROW_EXC'][] = [7, 8, 9];
			break;
			
		default:
			$arResult['COL_EXC'] = 4;
			$arResult['ROW_EXC'][] = [1, 2, 3];
			$arResult['ROW_EXC'][] = [4, 5, 6];
			break;
	}
	
	$_SESSION['COL_EXC'] = $arResult['COL_EXC'];
	
	// Compectations
	$arSelect = ['ID', 'IBLOCK_ID', 'NAME', 'PROPERTY_PRICE'];
	$arFilter = ['IBLOCK_ID'=>12, 'ACTIVE_DATE'=>'Y', 'ACTIVE'=>'Y', 'PROPERTY_MODEL'=>$arResult['ID']];
	$res = CIBlockElement::GetList(['SORT'=>'ASC'], $arFilter, false, false, $arSelect);
	
	for ( $i=1; $i<=10; $i++ ) {
		
		$cProps[] = 'PART_'.$i.'_TITLE';
		$cProps[] = 'PART_'.$i.'_LIST';
	}
	
	while ($ob = $res->GetNextElement()) {
		
		$tmp = $ob->GetFields();
		$add = true;
		foreach( $cProps as $p ) {
			
			if ( explode('_', $p)[2] == 'LIST' ) {
				
				if ( $add ) {
					
					$p_rs = CIBlockElement::GetProperty(12, $tmp['ID'], [], ['CODE'=>$p]);
					while ( $ob = $p_rs->GetNext() ) {
						
						$tp = [
							'VALUE' => $ob['VALUE'],
							'OPT' => ( $ob['DESCRIPTION'] ) ? true : false
						];
						$tmp['PROP'][explode('_', $p)[0].'_'.explode('_', $p)[1]]['LIST'][] = $tp;
					}
				}
				
			} else {
				
				$val = CIBlockElement::GetProperty(12, $tmp['ID'], [], ['CODE'=>$p])->Fetch()['VALUE'];
				if ( $val ) $tmp['PROP'][explode('_', $p)[0].'_'.explode('_', $p)[1]]['TITLE'] = $val;
					else $add = false;
			}
			
		}
		
		$arCompl[] = $tmp;
	}
	
	unset( $tmp );
	
	if ($arCompl) $arResult['COMPLECTATIONS']['HEAD']['TITLE'] = '';
	
	foreach ( $arCompl as $k => $c ) $arResult['COMPLECTATIONS']['HEAD']['LIST'][$k] = ['NAME'=>$c['NAME'], 'PRICE'=>(int)preg_replace("/[^0-9]/", '', $c['PROPERTY_PRICE_VALUE'])];
	
	foreach ( $arCompl[count($arCompl)-1]['PROP'] as $kPart => $vPart ) {
		
		$tmp['TITLE'] = $vPart['TITLE'];
		$tmp['IS_TITLE'] = true;
		
		$arResult['COMPLECTATIONS']['LIST'][] = $tmp;
		
		unset( $tmp );
		
		foreach ( $vPart['LIST'] as $kList => $vList ) {
			
			$tmp['TITLE'] = $vList['VALUE'];
			$tmp['IS_TITLE'] = false;
			
			foreach ( $arCompl as $kCompl => $vCompl ) {
				
				$finded = false;
				
				foreach ( $vCompl['PROP'][$kPart]['LIST'] as $vItem ) {
					
					if ( trim($vList['VALUE']) == trim($vItem['VALUE']) ) {
						
						$tmp['LIST'][$kCompl]['IS'] = true;
						$tmp['LIST'][$kCompl]['OPT'] = $vItem['OPT'];
						$finded = true;
					}
				}
				
				if ( !$finded ) {
					
					$tmp['LIST'][$kCompl]['IS'] = false;
					$tmp['LIST'][$kCompl]['OPT'] = false;
				}
			}
			
			$arResult['COMPLECTATIONS']['LIST'][] = $tmp;
		}
	}
	
	//YApp::sd( $_SERVER['REQUEST_PAGE'], true );
	if ( $_SERVER['REQUEST_PAGE'] ) {
		
		$GLOBALS['SEO']['ELEMENT_META_TITLE'] = ( $arResult['PAGES_TABS'][$_SERVER['REQUEST_PAGE']]['PROPS']['SEO_TITLE'] ) ?: $arResult['PAGES_TABS'][$_SERVER['REQUEST_PAGE']]['NAME'].' '.$arResult['PROPERTIES']['PAGE_TITLE']['VALUE'];
//		$APPLICATION->SetPageProperty("title", $title);
		
		if ( $arResult['PAGES_TABS'][$_SERVER['REQUEST_PAGE']]['PROPS']['SEO_DESCRIPTION'] ) $APPLICATION->SetPageProperty('description', $arResult['PAGES_TABS'][$_SERVER['REQUEST_PAGE']]['PROPS']['SEO_DESCRIPTION'] );
	}
?>