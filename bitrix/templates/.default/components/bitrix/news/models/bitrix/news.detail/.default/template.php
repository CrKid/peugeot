<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ( $arResult['PROPERTIES']['VIDEO']['VALUE'] ) { ?>
<div class="container">
  <div class="row">
	<video loop poster="<?=$arResult['DETAIL_PICTURE']['SRC']?>" width="100%" preload muted autobuffer controls>
	  <source type="video/mp4" src="<?=CFile::GetPath($arResult['PROPERTIES']['VIDEO']['VALUE'])?>">
	</video>
  </div>
</div>
<? } elseif ($arResult['DETAIL_PICTURE']) { ?>
<div class="container">
  <div class="row">
	<img class="w-100" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" />
  </div>
</div>
<? } ?>

<div class="container mb-5">
  <div class="row text-center model-tabs">
    <div class="col py-2 d-flex flex-wrap justify-content-center align-content-center <?=((!$_SERVER['REQUEST_PAGE'] || $_SERVER['REQUEST_PAGE']=='about')?'bg-p-lightwhite':'bg-p-llightwhite')?> bgh-p-lightwhite">
      <a href="<?=$arResult['DETAIL_PAGE_URL']?>" class="black"><?=$arResult['PROPERTIES']['MENU_TITLE']['VALUE']?></a>
    </div>
    <? foreach ($arResult['PAGES_TABS'] as $pageTab) { ?>
    <div class="col py-2 d-flex flex-wrap justify-content-center align-content-center <?=(($_SERVER['REQUEST_PAGE']==$pageTab['CODE'])?'bg-p-lightwhite':'bg-p-llightwhite')?> bgh-p-lightwhite">
      <a href="<?=$arResult['DETAIL_PAGE_URL']?><?=$pageTab['CODE']?>/" class="black"><?=$pageTab['NAME']?></a>
    </div>
    <? } ?>
  </div>
</div>

<? include ( $_SERVER['REQUEST_PAGE'] && $_SERVER['REQUEST_PAGE'] != 'about') ? 'view.php' : 'main.php'; ?>