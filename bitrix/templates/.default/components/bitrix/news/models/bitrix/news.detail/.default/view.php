<?php $viewPage = YApp::Safe($_SERVER['REQUEST_PAGE']); ?>

<?php if ( $viewPage == 'complectations' && $arResult['COMPLECTATIONS'] ) { // ?>
    
<div class="container py-5">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1><?=$arResult['PAGES_TABS'][$viewPage]['NAME']?> <?=$arResult['PROPERTIES']['PAGE_TITLE']['VALUE']?></h1>
    </div>
  </div>
</div>
    
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <table class="table table-sm table-hover">
        <thead>
          <tr>
            <th style="width: <?=100-12*count($arResult['COMPLECTATIONS']['HEAD']['LIST'])?>%;"></th>
            <?php foreach ( $arResult['COMPLECTATIONS']['HEAD']['LIST'] as $arItem) { ?>
            <th style="width: 12%;" class="text-center">
              <?=strtoupper($arItem['NAME'])?><br />
              <small>от</small><br />
              <?=number_format($arItem['PRICE'], 0, '', ' ');?> ₽
            </th>
            <?php } // foreach ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ( $arResult['COMPLECTATIONS']['LIST'] as $arItem ) { ?>
          <tr>
            <?php if ( $arItem['IS_TITLE'] ) { ?>
            <td colspan="<?=1+count($arResult['COMPLECTATIONS']['HEAD']['LIST'])?>" style="padding-top: 25px; text-transform: uppercase;"><strong><?=$arItem['TITLE']?></strong></td>
            <?php } else { ?>
            <td><?=$arItem['TITLE']?></td>
            <?php foreach ( $arItem['LIST'] as $v ) { ?>
            <td class="text-center"><?=(($v['OPT'])?'ОПЦИЯ':(($v['IS'])?'<svg xmlns="http://www.w3.org/2000/svg"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-plus"></use></svg>':''))?></td>
            <?php } // foreach ?>
            <?php } ?>
          </tr>
          <?php } // foreach ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php } else { ?> 
    
<div class="container py-5">
  <div class="row">
    <div class="col-md-12 text-center">
      <h1><?=$arResult['PAGES_TABS'][$viewPage]['NAME']?> <?=$arResult['PROPERTIES']['PAGE_TITLE']['VALUE']?></h1>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12 text-center">
      <p><?=$arResult['PAGES_TABS'][$viewPage]['PREVIEW_TEXT']?></p>
    </div>
  </div>
</div>
    
    
<?php $PAGE = $arResult['PAGES_TABS'][$viewPage]['PROPS']; $PAGE_COUNT = count($PAGE) / 6; ?>
    
<div class="container">
  <div class="row">
  
    <?php for ($i = 1; $i <= $PAGE_COUNT; $i++) { ?>
      
      <?php if ((int)$PAGE['SECTION_0'.$i.'_COLUMN'] == 1 || (int)$PAGE['SECTION_0'.$i.'_COLUMN'] == 0) { ?>
        
        <?php if ($viewed[$i] !='Y') { ?>
        <div class="col-md-12 py-3"><h3 class="text-center"><?=$PAGE['SECTION_0'.$i.'_TITLE']?></h3></div>
      
        <?php if ($PAGE['SECTION_0'.$i.'_SLIDER'] == 'ON') { ?>
        
          <?php $sliderCol = ($PAGE['SECTION_0'.$i.'_FULLWIDTHSLIDER'] == 'ON') ? 12 : 6; ?>          
        
          <div class="col-md-<?=$sliderCol?>" data-section="<?=$i?>">
            <?php if (count($PAGE['SECTION_0'.$i.'_PICS']) > 1) { ?>
            <div class="swiper-container swiper-section-<?=$i?>" style="height: auto;">
              <div class="swiper-wrapper">
                <?php foreach ($PAGE['SECTION_0'.$i.'_PICS'] as $pic) { ?>
                <div class="swiper-slide">
                  <img class="w-100" src="<?=CFile::GetPath($pic)?>" />
                </div>
                <?php } ?>
              </div>
              <!-- Add Arrows -->
              <div class="swiper-arrow swiper-next">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
                </svg>
              </div>
              <div class="swiper-arrow swiper-prev">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
                </svg>
              </div>
            </div>
            <?php
				$GLOBALS['SWIPER'][] = [
					'VAR' => 'JSApp.Swipers.ModelViewSection'.$i,
					'CLASS' => '.swiper-section-'.$i,
					'NAV' => [
						'NEXT' => '[data-section="'.$i.'"] .swiper-next',
						'PREV' => '[data-section="'.$i.'"] .swiper-prev'
					],
					'LOOP' => true,
				];
			?>
            <?php } else { ?>
            <img class="w-100" src="<?=CFile::GetPath($PAGE['SECTION_0'.$i.'_PICS'][0])?>" />
            <?php } ?> 
          </div>
          <div class="col-md-<?=$sliderCol?>"><?=$PAGE['SECTION_0'.$i.'_TEXT']?></div>
        
        <?php } else { ?>
        <div class="col-md-12">
          <?php if ($PAGE['SECTION_0'.$i.'_PICS'][0]) { ?>
          <?php foreach ($PAGE['SECTION_0'.$i.'_PICS'] as $pic) { ?>
          <img src="<?=CFile::GetPath($pic)?>" style="width: calc(<?=100/count($PAGE['SECTION_0'.$i.'_PICS']);?>% - 2px); float: left; margin-right: 2px; margin-bottom: 1em;" />
          <?php } ?>
          <?php } ?>
        </div>
        <div class="col-md-12"><?=$PAGE['SECTION_0'.$i.'_TEXT']?></div>
        <?php } ?>
        
        <?php $viewed[$i] = 'Y'; ?>
        <?php } ?>
      <?php } else { ?>
          
          <?php if ($viewed[$i] !='Y') { ?>
          <br /><br />
          <?php for ($k = $i; $k < $i + (int)$PAGE['SECTION_0'.$i.'_COLUMN']; $k++) { ?>
          
            <?php if ($viewed[$k] !='Y') { ?>
              
              <div class="col-md-<?=12/(int)$PAGE['SECTION_0'.$i.'_COLUMN']?>">
                <?php if ($PAGE['SECTION_0'.$k.'_SLIDER'] == 'ON') { ?>
                <div class="swiper-container swiper-section-<?=$i?>" style="height: auto;">
                  <div class="swiper-wrapper">
                    <?php foreach ($PAGE['SECTION_0'.$i.'_PICS'] as $pic) { ?>
                    <div class="swiper-slide">
                      <img class="w-100" src="<?=CFile::GetPath($pic)?>" />
                    </div>
                    <?php } ?>
                  </div>
                  <!-- Add Arrows -->
                  <div class="swiper-arrow swiper-next">
                    <svg xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-right"></use>
                    </svg>
                  </div>
                  <div class="swiper-arrow swiper-prev">
                    <svg xmlns="http://www.w3.org/2000/svg">
                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-arrow-left"></use>
                    </svg>
                  </div>
                </div>
                <?php
                    $GLOBALS['SWIPER'][] = [
                        'VAR' => 'JSApp.Swipers.ModelViewSection'.$i,
                        'CLASS' => '.swiper-section-'.$i,
                        'NAV' => [
                            'NEXT' => '[data-section="'.$i.'"] .swiper-next',
                            'PREV' => '[data-section="'.$i.'"] .swiper-prev'
                        ],
                        'LOOP' => true,
                    ];
                ?>
                <?php } else { ?>
                <br />
                <?php if ($PAGE['SECTION_0'.$k.'_PICS']) { ?>
                <?php foreach ($PAGE['SECTION_0'.$k.'_PICS'] as $pic) { ?>
                <img src="<?=CFile::GetPath($pic)?>" style="width: calc(<?=100/count($PAGE['SECTION_0'.$k.'_PICS']);?>% - 2px); float: left; margin-right: 2px; margin-bottom: 1em;" />
                <?php } ?>
                <div class="clear"></div>
                <?php } ?>
                <h3><?=$PAGE['SECTION_0'.$k.'_TITLE']?></h3>
                <?php } ?>
                <?=$PAGE['SECTION_0'.$k.'_TEXT']?>
              </div>
              <?php $viewed[$k] = 'Y'; ?>
              
            <?php } ?>
            
          <?php } ?>
          <div class="clear"><br /></div>
      <?php } ?>
      
      <?php } ?>
      
    <?php } ?>
  
  
  </div>
</div>
<?php } // if complectations ?>

<?php
	$APPLICATION->SetTitle( $arResult['PAGES_TABS'][$viewPage]['NAME'].' '.$arResult['PROPERTIES']['PAGE_TITLE']['VALUE'] );
?>