<?php
	
	foreach ( $arResult['ITEMS'] as $k => $arItem ) {
		
		$sec_id = $arItem['IBLOCK_SECTION_ID'];
		if ( !$arResult['SECTIONS'][$sec_id] ) $arResult['SECTIONS'][$sec_id] = CIBlockSection::GetByID($sec_id)->GetNext();
		$arResult['SECTIONS'][$sec_id]['ITEMS'][] = $k;
	}