<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container py-5">
  <?php foreach ( $arResult['SECTIONS'] as $arSec ) { ?>
  <div class="row mb-5">
    <div class="col-md-12 text-center mb-3"><div class="h2 font-weight-normal text-uppercase"><?=$arSec['NAME']?></div></div>
    <?php foreach ( $arSec['ITEMS'] as $i ) {
		$arItem = $arResult['ITEMS'][$i]; ?>
        <div class="col-md-3 mb-3 text-center">
          <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><img class="w-75" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" /></a>
          <hr />
          <div class="h5 text-left"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['PROPERTIES']['MENU_TITLE']['VALUE']?></a></div>
        </div>
    <?php } // foreach elements ?>
  </div>
  <?php } // foreach sections ?>
</div>