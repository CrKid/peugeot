<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container pb-5">
  <div class="row">
    <div class="col-md-12">
      <img class="w-100" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" title="<?=$arResult['NAME']?>" />
      <h1 class="font-weight-light text-uppercase text-center py-3"><?=$arResult['NAME']?></h1>
      <?=$arResult['DETAIL_TEXT']?>
    </div>
  </div>
</div>
