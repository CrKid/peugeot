<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
  <div class="row mt-5 mb-3">
    <div class="col-md-12 text-center"><div class="h2 font-weight-normal text-uppercase">Новости</div></div>
  </div>
  <div class="row mb-5">
    <?php foreach ( $arResult['ITEMS'] as $arItem ) { ?>
    <?php
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="col-md-4 mb-3">
      <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
        <img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" class="card-img-top" alt="<?=$arItem['NAME']?>">
      </a>
      <h5 class="my-3"><?=$arItem['NAME']?></h5>
      <p><a href="<?=$arItem['DETAIL_PAGE_URL']?>">Подробнее</a></p>
    </div>
    <?php } // foreach ?>
  </div>
  
  <?php if($arParams["DISPLAY_BOTTOM_PAGER"]) { ?>
  <div class="row mb-5">
    <div class="col-md-12"><?=$arResult["NAV_STRING"]?></div>
  </div>
  <?php } ?>
</div>