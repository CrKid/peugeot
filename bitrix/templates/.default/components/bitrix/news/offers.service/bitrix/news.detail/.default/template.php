<?php if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <img class="w-100" src="<?=$arResult['DETAIL_PICTURE']['SRC']?>" title="<?=$arResult['NAME']?>" />
      <h1 class="font-weight-light text-uppercase text-center py-3"><?=$arResult['NAME']?></h1>
      <?=$arResult['DETAIL_TEXT']?>
    </div>
  </div>
</div>

<?php if ( $arResult['PROPERTIES']['USE_FORM']['VALUE_XML_ID'] ) { ?>
<?php CModule::IncludeModule('form'); $arForm = CForm::GetBySID($arResult['PROPERTIES']['USE_FORM']['VALUE_XML_ID'])->Fetch(); ?>
<div class="py-4 bg-p-lightwhite">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-center">
        <h2 class="font-weight-light text-uppercase c-p-ddarkblue"><?=$arForm['NAME']?></h2>
        <p><?=$arResult['PROPERTIES']['FORM_TEXT']['VALUE']?></p>
      </div>
      <div class="col-md-12">
        <?php
            
            $APPLICATION->IncludeComponent(
                "bitrix:form.result.new", 
                "form.inline", 
                array(
                    "CACHE_TIME" => "0",
                    "CACHE_TYPE" => "A",
                    "CHAIN_ITEM_LINK" => "",
                    "CHAIN_ITEM_TEXT" => "",
                    "EDIT_URL" => "result_edit.php",
                    "IGNORE_CUSTOM_TEMPLATE" => "N",
                    "LIST_URL" => "result_list.php",
                    "SEF_MODE" => "N",
                    "SUCCESS_URL" => "",
                    "USE_EXTENDED_ERRORS" => "N",
                    "WEB_FORM_ID" => $arForm['ID'],
                    "COMPONENT_TEMPLATE" => "forms.modal",
                    "VARIABLE_ALIASES" => array(
                        "WEB_FORM_ID" => "WEB_FORM_ID",
                        "RESULT_ID" => "RESULT_ID",
                    )
                ),
                false
            );
        ?>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<div class="container">
  <div class="row py-5">
    <?php if ( $arResult['PROPERTIES']['DISCLAMER']['~VALUE']['TEXT'] ) { ?>
    <div class="col-md-12">
      <small><?=$arResult['PROPERTIES']['DISCLAMER']['~VALUE']['TEXT']?></small>
    </div>
    <?php } // if USE_HOT ?>
  </div>
</div>
