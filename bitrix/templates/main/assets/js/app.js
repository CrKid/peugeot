'use strict';

$('input[type="phone"]').inputmask({'mask': '+7 (999) 999-99-99', showMaskOnHover: false});
JSApp.Calendar = new flatpickr('input[data="DATE"]', {
    locale: "ru",
	altInput: true
});
JSApp.ScroleTo = function( scrole ) {
	
	$('html, body').animate({ scrollTop: $('div[data-scrole="' + scrole + '"]').offset().top }, 500);
}

$(document).on('click', 'li[role="showNav"]', function() {
	
	$('.nav-section[data-section="'+$(this).data('section')+'"]').siblings('.nav-section').slideUp(100);
	if ( $('.nav-section').not(':visible') ) $('.nav-cover').fadeIn(500);
	if ( $('.nav-section:visible').data('section') == $(this).data('section') ) $('.nav-cover').fadeOut(500);
	$('.nav-section[data-section="'+$(this).data('section')+'"]').slideToggle(500);
	
	return false; 
});
$(document).on('click', 'a.close-section', function() {
	
	$(this).parent().slideUp(500);
	$('.nav-cover').fadeOut(500);
	
	return false;
});

$(document).on('click', '.nav-cover', function() {
	
	$('.nav-section').slideUp(500);
	$('.nav-cover').fadeOut(500);
	
	return false;
});

$(document).on('click', 'a[role="disclamer"]', function() {
	
	var attr = ( $(this).find('use').attr('xlink:href') == '#icon-arrow-down') ? '#icon-arrow-up' : '#icon-arrow-down';
	$(this).find('use').attr('xlink:href', attr);
	$(this).siblings('div[role="disclamer"]').slideToggle(500);
	
	return false;
});

$(document).on('click', 'div.rules', function() {
	
	$(this).removeClass('c-yaerror');
	$(this).toggleClass('checked');
	var attr = ( $(this).find('use').attr('xlink:href') == '#icon-checked' ) ? '#icon-unchecked' : '#icon-checked';
	$(this).find('use').attr('xlink:href', attr);
	if ( attr == '#icon-unchecked') $(this).addClass('c-yaerror');
	
});

$(document).on('click', '[role="model-tab"]', function() {
	
	$(this).siblings('[role="model-tab"]').removeClass('active');
	$(this).siblings('[role="model-tab"]').find('use').attr('xlink:href', '#icon-plus');
	$(this).toggleClass('active');
	var attr = ( $(this).find('use').attr('xlink:href') == '#icon-plus' ) ? '#icon-minus' : '#icon-plus';
	$(this).find('use').attr('xlink:href', attr);
	
	$('[role="model-tab-content"][data-tab="'+$(this).data('tab')+'"]').slideToggle(500);
	$('[role="model-tab-content"][data-tab="'+$(this).data('tab')+'"]').siblings('[role="model-tab-content"]').slideUp(500);
	
	if ( $(this).hasClass('active') ) JSApp.ScroleTo( $(this).data('tab') );
});
$(document).on('click', '[role="scrole"]', function() { JSApp.ScroleTo( $(this).data('scrole') ) });

$(document).on('click', '[role="showNavM"]', function() {
	
	$('.nav-section-m').fadeToggle(500);
	var attr = ( $(this).find('use').attr('xlink:href') == '#icon-burger' ) ? '#icon-burger-close' : '#icon-burger';
	$(this).find('use').attr('xlink:href', attr);
});

$(document).on('click', '[role="show-sub"]', function() {
	
	var level = $(this).data('level');
	var sect = $(this).data('sectm');
	var upsect = $(this).data('upsect');
	$('.sub-level[data-level="'+level+'"][data-sectm="'+sect+'"][data-upsect="'+upsect+'"]').siblings('.sub-level').css('z-index', '20');
	$('.sub-level[data-level="'+level+'"][data-sectm="'+sect+'"][data-upsect="'+upsect+'"]').css('z-index', '25').addClass('active');
});
$(document).on('click', '[role="show-up"]', function() {
	
	$(this).parent().removeClass('active');
});


$(document).on('click', '[role="SendForm"]', function() {
	
	JSApp.Send.Data = {};
	JSApp.Send.Layer = {};
	JSApp.Send.Status = true;
	
	var Form = $(this).parents('form');
	var Success = $(Form).siblings('.alert-success');
	var Error = $(Form).siblings('.alert-danger');
	var Event = $(Form).data('event') || false;
	
	$(Form).children('.form-cover').show();
	
	$(Form).find('input, select, textarea').each( function(i, e) {
		
		JSApp.Send.Data[$(e).attr('name')] = $(e).val();
		
		$(e).removeClass('is-invalid');
		if ( $(e).attr('required') && !$(e).val() ) {
			
			JSApp.Send.Status = false;
			$(e).removeClass('is-invalid').addClass('is-invalid');
		}
	});
	
	$(Form).find('.rules').each( function(i, e) {
		
		if ( $(this).find('use').attr('xlink:href') == '#icon-unchecked' ) JSApp.Send.Status = false;
	});
	
	
	if ( JSApp.Send.Status ) {
		
		console.log(JSApp.Send.Data);
		
		$.ajax({
            type: "POST",
            url: '/ajax/',
            data: JSApp.Send.Data,
            success: function(data) {

                var res = JSON.parse(data);

                if ( res.status == 'success' ) {

                    $(Success).slideDown(500);
					$(Form).slideUp(500)
                    $(Error).hide();
					
					
					if (typeof Matomo != 'undefined') _paq.push(["trackEvent", 'Формы', $(Form).attr('name'), 'Отправка']);
						
					JSApp.Send.Layer = {
						'event': 'FormSubmission',
						'eventCategory': Event,
						'eventAction': 'submit',
						'eventLabel': JSApp.Send.Data.MODEL || null
					};
					dataLayer.push( JSApp.Send.Layer );

                    
                    var CallTouchURL = 'https://api-node7.calltouch.ru/calls-service/RestAPI/requests/20905/register/';
                    CallTouchURL += '?subject=Формы сайта - '+$(Form).attr('name');
                    CallTouchURL += '&sessionId='+window.call_value_11;
                    CallTouchURL += '&fio='+JSApp.Send.Data.NAME;
                    CallTouchURL += '&phoneNumber='+JSApp.Send.Data.PHONE.replace(/[^\d;]/g, '');
					
                    $.get( CallTouchURL );
					

                } else {

                    $(Error).slideDown(500);
					setTimeout( function() { $(Error).slideUp(500) }, 5000);
                }
            },
            error: function() {

                $(Error).slideDown(500);
				setTimeout( function() { $(Error).slideUp(500) }, 5000);
            }
        });
	}
	
	$(Form).children('.form-cover').hide();
	
	return false;
});

