var SwBanner = new Swiper('.swiper-banner', {
	navigation: {
		nextEl: '.swiper-banner .swiper-next',
		prevEl: '.swiper-banner .swiper-prev',
	},
	loop: true,
});

var SwMModels = new Swiper('.swiper-mainmodels', {
	navigation: {
		nextEl: '.swiper-next[role="swiper-mainmodels-next"]',
		prevEl: '.swiper-prev[role="swiper-mainmodels-prev"]',
	},
	loop: true,
	slidesPerView: 3,
    spaceBetween: 0,
});