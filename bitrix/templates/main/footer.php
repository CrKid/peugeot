  <footer class="py-5 bg-p-lightwhite">
    
    <div class="container">
      <div class="row">
        <?php // 2 x <div class="col-md-4"></div> ?>
        <?php $APPLICATION->IncludeComponent(
			"bitrix:menu", 
			"bottom", 
			array(
				"ALLOW_MULTI_SELECT" => "N",
				"CHILD_MENU_TYPE" => "",
				"DELAY" => "N",
				"MAX_LEVEL" => "1",
				"MENU_CACHE_GET_VARS" => array(
				),
				"MENU_CACHE_TIME" => "3600",
				"MENU_CACHE_TYPE" => "N",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"ROOT_MENU_TYPE" => "bottom",
				"USE_EXT" => "N",
				"COMPONENT_TEMPLATE" => "bottom"
			),
			false
		);?>
        <div class="col-md-4">
          <a href="#FORM_TEST_DRIVE" class="but but-blue mb-2 d-block">Записаться на тест-драйв</a>
          <a href="#FORM_CALLBACK" class="but but-white d-block">Заказать обратный звонок</a>
        </div>
        <?php
			// Режим работы
			$rs = CIBlockElement::GetProperty(6, 24, 'sort', 'asc', ['CODE'=>'SCHEDULE_SALE']);
			$GLOBALS['SETTINGS']['WORK_SALES'] = $rs->GetNext()['VALUE'];
			$rs = CIBlockElement::GetProperty(6, 24, 'sort', 'asc', ['CODE'=>'SCHEDULE_SERVICE']);
			$GLOBALS['SETTINGS']['WORK_SERVICE'] = $rs->GetNext()['VALUE'];
		?>
        <div class="col-md-4 mt-4">
          <h5 class="font-weight-normal text-uppercase">Адрес</h5>
          <hr />
          <p><?=$GLOBALS['SETTINGS']['ADDRESS']?></p>
          <p>
            Режим работы:<br />
            Отдел продаж: <?=$GLOBALS['SETTINGS']['WORK_SALES']?><br />
            Отдел сервиса: <?=$GLOBALS['SETTINGS']['WORK_SERVICE']?>
          </p>
        </div>
        <div class="col-md-4 mt-4">
          <h5 class="font-weight-normal text-uppercase">Телефон</h5>
          <hr />
          <div class="phone peugeot_call_phone_2"><a href="tel:+<?=YApp::phoneIn($GLOBALS['SETTINGS']['PHONE'])?>"><?=YApp::phoneOut($GLOBALS['SETTINGS']['PHONE'])?></a></div>
        </div>
        <div class="col-md-4 mt-4 social">
          <?php
		  $o = ['SORT'=>'ASC'];
		  $f = ['IBLOCK_ID'=>5, 'SECTION_ID'=>3, 'ACTIVE'=>'Y'];
		  $s = ['ID', 'PROPERTY_LINK', 'PROPERTY_ICON', 'PROPERTY_TARGET'];
		  
		  $rs = CIBlockElement::GetList( $o, $f, false, false, $s );
		  while( $ob = $rs->GetNextElement() ) {
		      $s = $ob->GetFields(); ?>
              <a href="<?=$s['PROPERTY_LINK_VALUE']?>" target="<?=$s['PROPERTY_TARGET_VALUE']?>" class="mr-1">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#<?=$s['PROPERTY_ICON_VALUE']?>"></use>
                </svg>
              </a>
          <?php } // while ?>
        </div>
      </div>
    </div>
    
  </footer>
  
  <footer class="container py-3">
    <div class="row">
      <div class="col-md-12 text-center text-uppercase">
        <ul class="list-inline">
          <li class="list-inline-item px-3"><a href="#">Правовая информация</a></li>
          <li class="list-inline-item px-3"><a href="#">Персональные данные</a></li>
          <li class="list-inline-item px-3"><a href="#">Использование cookie</a></li>
        </ul>
      </div>
    </div>
  </footer>
  
  <?php include __DIR__.'/assets/svg/app.php'; ?>
  <?php include __DIR__.'/include/_modals.php'; ?>
  
  
  
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js" integrity="sha256-HQCkPjsckBtmO60xeZs560g8/5v04DvOkyEo01zhSpo=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js" integrity="sha256-uckMYBvIGtce2L5Vf/mwld5arpR5JuhAEeJyjPZSUKY=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js" integrity="sha256-tR7fz2fCeycqI9/V8mL9nyTc4lI14kg2Qm6zZHuupxE=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.2/flatpickr.min.js" integrity="sha256-VmQJqA6rQrsqI4z+CdrEtRDd5VIxAHLaSHcjtj/Rxu0=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.2/l10n/ru.js" integrity="sha256-6j3mL9IbSX+E0ajD13BUZuSo8WyjLXOWb9/3nmicTog=" crossorigin="anonymous"></script>
  
  <script src="<?=YApp::MT()?>/assets/js/app.js?<?=md5_file(__DIR__.'/assets/js/app.js')?>" charset="utf-8"></script>
  <?php include __DIR__.'/include/_swiper.php'; ?>
  
  <?php include __DIR__.'/include/_body.end.php'; ?>
  
  </body>
</html>
<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>