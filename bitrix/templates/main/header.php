<?php
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	CModule::IncludeModule('iblock');
	include __DIR__.'/include/_settings.global.php';
?>
<!doctype html>
<html lang="ru">
  <head>
    
	<?php include __DIR__.'/include/_head.start.php'; ?>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="author" content="Boretscy A">
    
    <title><?php // $APPLICATION->ShowProperty('title', false);?><?php $APPLICATION->ShowTitle(); ?></title>
    
    <?php $APPLICATION->ShowHead();?>
    <link href="<?=YApp::MT()?>/assets/fonts/fonts.css" rel="stylesheet" />
	<link href="<?=YApp::MT()?>/assets/css/bootstrap.min.css?<?=md5_file(__DIR__.'/assets/css/bootstrap.min.css')?>" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" integrity="sha256-XwfUNXGiAjWyUGBhyXKdkRedMrizx1Ejqo/NReYNdUE=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.css" integrity="sha256-NIskOH7eNIjyJFpUeh3DlE9n1amcG/bzVKbWxuIgiH4=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal-default-theme.min.css" integrity="sha256-iJlvlQFv31232zI/zrsL/jbuubLWWr/Bv99d+XfaC7Y=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.2/flatpickr.min.css" integrity="sha256-Zh4AVwxlwpUo2c5u4Z5emTmYZxbCk972ewf4tqGRsBg=" crossorigin="anonymous" />
	
    
    <link href="<?=YApp::MT()?>/assets/css/app.css?<?=md5_file(__DIR__.'/assets/css/app.css')?>" rel="stylesheet" />
    <link href="<?=YApp::MT()?>/assets/css/swiper.css?<?=md5_file(__DIR__.'/assets/css/swiper.css')?>" rel="stylesheet" />
    <link href="<?=YApp::MT()?>/assets/css/media.css?<?=md5_file(__DIR__.'/assets/css/media.css')?>" rel="stylesheet" />
	
    <?php include __DIR__.'/include/_favicon.php'; ?>
    
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?=$_SERVER['REQUEST_SCHEME']?>://<?=$_SERVER['HTTP_HOST']?>/" />
    <meta property="og:title" content="<?php $APPLICATION->ShowProperty('title');?>" />
    <meta property="og:description" content="<?php $APPLICATION->ShowProperty('description');?>" />
    <meta property="og:image" content="<?=$_SERVER['REQUEST_SCHEME']?>://<?=$_SERVER['HTTP_HOST']?><?=YApp::MT()?>/assets/images/og_logo.jpg"  />
    
    <script src="<?=YApp::MT()?>/assets/js/init.js?<?=md5_file(__DIR__.'/assets/js/init.js')?>" charset="utf-8"></script>
    
    <?php include __DIR__.'/include/_head.end.php'; ?>
    
  </head>

  <body>
    
	<?php include __DIR__.'/include/_body.start.php'; ?>
	<?php $APPLICATION->ShowPanel();?>
	
    <?php $APPLICATION->IncludeComponent(
        "bitrix:menu", 
        "top", 
        array(
            "ALLOW_MULTI_SELECT" => "N",
            "CHILD_MENU_TYPE" => "",
            "DELAY" => "N",
            "MAX_LEVEL" => "1",
            "MENU_CACHE_GET_VARS" => array(
            ),
            "MENU_CACHE_TIME" => "3600",
            "MENU_CACHE_TYPE" => "N",
            "MENU_CACHE_USE_GROUPS" => "Y",
            "ROOT_MENU_TYPE" => "top",
            "USE_EXT" => "N",
            "COMPONENT_TEMPLATE" => "top"
        ),
        false
    );?>