<?php
if ( CModule::IncludeModule('form') ) {
	
	$f = ['SITE'=>'s1'];
	$rs = CForm::GetList($by='s_id', $order='desc', $f);
	while ($ob = $rs->Fetch()) $arForms[] = $ob;
}
?>

<?php foreach ( $arForms as $arItem ) { ?>
<div class="remodal border b-p-blue" data-remodal-id="<?=$arItem['SID']?>">
  <button data-remodal-action="close" class="remodal-close"></button>
  <? $APPLICATION->IncludeComponent(
      "bitrix:form.result.new", 
      "form.modal", 
      array(
          "CACHE_TIME" => "0",
          "CACHE_TYPE" => "A",
          "CHAIN_ITEM_LINK" => "",
          "CHAIN_ITEM_TEXT" => "",
          "EDIT_URL" => "result_edit.php",
          "IGNORE_CUSTOM_TEMPLATE" => "N",
          "LIST_URL" => "result_list.php",
          "SEF_MODE" => "N",
          "SUCCESS_URL" => "",
          "USE_EXTENDED_ERRORS" => "N",
          "WEB_FORM_ID" => $arItem['ID'],
          "COMPONENT_TEMPLATE" => "forms.modal",
          "VARIABLE_ALIASES" => array(
              "WEB_FORM_ID" => "WEB_FORM_ID",
              "RESULT_ID" => "RESULT_ID",
          )
      ),
      false
  );?>
</div>
<?php } ?>