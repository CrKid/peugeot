<?php
	
	foreach ( $GLOBALS['SWIPER'] as $arSwiper ) {
		
		?>
        <script>
        	<?=$arSwiper['VAR']?> = new Swiper('<?=$arSwiper['CLASS']?>', {
				<?php if ( $arSwiper['NAV'] ) { ?>
				navigation: {
					nextEl: '<?=$arSwiper['NAV']['NEXT']?>',
					prevEl: '<?=$arSwiper['NAV']['PREV']?>',
				},
				<?php } ?>
				<?php if ( $arSwiper['AUTO'] ) { ?>
				autoplay: {
					delay: 3500,
					disableOnInteraction: true,
				},
				<?php } ?>
				<?php if ( $arSwiper['PAG'] ) { ?>pagination: { el: '<?=$arSwiper['PAG']?>', clickable: true},<?php } ?>
				<?php if ( $arSwiper['LOOP'] ) { ?>loop: true,<?php } ?>
				<?php if ( $arSwiper['SLIDES'] ) { ?>slidesPerView: <?=$arSwiper['SLIDES']?>,<?php } ?>
				<?php if ( $arSwiper['SPACE'] ) { ?>spaceBetween: <?=(int)$arSwiper['SPACE']?>,<?php } ?>
			});
		
        </script>
		<?php	
	}
	
	if ( $APPLICATION->GetCurPage(false) === '/' ) {
		
		?>
        <script>
        	JSApp.Swipers.SwBanner.on('slideChange', function() {
				//console.log( $('.swiper-banner [data-swiper-slide-index="'+JSApp.Swipers.SwBanner.realIndex+'"]').data('link') );
				$('a[role="banner-mobile"]').attr('href', $('.swiper-banner [data-swiper-slide-index="'+JSApp.Swipers.SwBanner.realIndex+'"]').data('link') );
			});
        </script>
        <?php	
	}
?>