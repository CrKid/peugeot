<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Специальная программа от &laquo;PSA Group&raquo; и Росбанк Лизинг");
?>

<div class="container banner">
    <div class="row pb-3 "> 
	<img src="/upload/zlchl.351779.32.jpg" style="width: 100%;">
	<div class="col-sm-12 col-md-12 pt-3 pb-3">
		<h3 style="text-align: center;">СПЕЦИАЛЬНАЯ ПРОГРАММА ОТ «PSA GROUP» И РОСБАНК ЛИЗИНГ</h3>
		<p><strong>В РАМКАХ СОВМЕСТНОЙ ПРОГРАММЫ ПРИОБРЕТЕНИЕ городских АВТОМОБИЛЕЙ Peugeot стало еще более выгодным! </strong></p>
		<ul>
		<li>Росбанк Лизинг – официальный провайдер программы PEUGEOT Leasing*;</li>
		<li>Сниженный процент удорожания**;</li>
		<li>Ускоренное рассмотрение заявки***;</li>
		<li>Без предоставления финансовой отчетности;</li>
		<li>Отсутствие комиссий;</li>
		<li>Срок действия программы до 31.12.2019;</li>
		<li>Действие программы – на территории всей России.</li>
</ul>
	</div>
</div>
</div>	

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>