<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Запрос на покупку | Компания Юг-Авто - официальный диллер Peugeot в Краснодаре");
?><div class="container py-5">
	<div class="row">
		<div class="col-md-12">
			<h1>Запрос на покупку</h1>
		</div>
		<div class="col-md-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"form.inline",
	Array(
		"CACHE_TIME" => "0",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "2"
	)
);?>
		</div>
	</div>
</div>
 <br>
 <br><? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>