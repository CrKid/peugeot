<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Trade-in | Компания Юг-Авто - официальный диллер Peugeot в Краснодаре");
?>

<div class="container">

<img src="/upload/models/trade-in/slide4.jpg" class="title-img">
<h1 class="pt-3 pb-3">Trade-in</h1>
 <div id="accordion" pb-3>
        <div class="card">
          <div class="card-header" id="headingTwo">
            <h5 class="mb-0">
              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                ПОДРОБНЕЕ О ПРОГРАММЕ
              </button> <span class="caret"></span>
            </h5>
          </div>
      
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
            <div class="card-body">
                <h3>ГАРАНТИЯ</h3>
                <p>2 типа гарантии: «КОМФОРТ» сроком 6 месяцев «ПРЕМИУМ» сроком 12 месяцев</p>
                <h3>ГАРАНТИЯ В РАМКАХ ПРОГРАММЫ «PEUGEOT C ПРОБЕГОМ» ПОКРЫВАЕТ СЛЕДУЮЩИЕ УЗЛЫ И АГРЕГАТЫ*:</h3>
                <ul>
                    <li>Двигатель</li>
                    <li>КПП</li>
                    <li>Трансмиссия</li>
                    <li>Рулевое управление</li>
                    <li>Электрика и электронные системы</li>
                </ul>
                <p>В случае поломки Вам надо связаться с Вашим дилерским центром Peugeot. Затем Вам необходимо предоставить автомобиль на сервисную станцию. *Точный список марок автомобилей с пробегом и условия по гарантии Вы можете уточнить у сотрудников официальных Дилеров Peugeot.</p>
                
                <h3>100 ТОЧЕК КОНТРОЛЯ</h3>
                <p>Перед тем как включить автомобиль в программу PEUGEOT С ПРОБЕГОМ, специалисты дилерского центра тщательно проверят его по 100 пунктам. Техническое состояние каждого автомобиля, проверенного по стандартам программы PEUGEOT С ПРОБЕГОМ, подтверждается официальным Актом диагностики.</p>
                
                <h3>СТРАХОВАНИЕ</h3>
                <p>Воспользуйтесь специальными условиями страхования. В рамках программы PEUGEOT С ПРОБЕГОМ мы разработали для Вас специальные условия по страховке, максимально приближенные к условиям на новые автомобили.</p>
                
                <h3>ТРЕЙД-ИН</h3>
                <p>Трейд-ин - обмен бывшего в эксплуатации автомобиля на новый автомобиль. Воспользуйтесь специальным предложением и получите дополнительную выгоду при покупке нового автомобиля Peugeot.</p>
                
                <h3>ПРОДЛЕННАЯ ГАРАНТИЯ</h3>
                <p>За дополнительную плату может быть увеличен срок действия Технической Гарантии до 24-х месяцев и/или перечень покрываемых Технической Гарантией деталей (полный перечень покрываемых Технической Гарантией деталей, указан в Гарантийном сертификате на автомобиль). АВТОМОБИЛИ ГОТОВЫ К ПРОДАЖЕ Приобретая автомобиль по программе PEUGEOT С ПРОБЕГОМ, Вы можете быть уверены в высоком уровне комфорта и безопасности как при покупке нового автомобиля Peugeot.</p>
                
                <h3>КРЕДИТНОЕ ПРЕДЛОЖЕНИЕ</h3>
                <p>Воспользуйтесь специальными условиями кредитования. В рамках программы PEUGEOT С ПРОБЕГОМ мы разработали для Вас специальные условия по кредиту на данные автомобили, максимально приближенные к условиям на новые автомобили.</p>
            </div>
          </div>
		</div>
    </div>



<img src="/upload/models/trade-in/pg_diagram.jpg" class="title-img pt-3" />

<div class="row no-p-m">
<div class="col-md-4">
<div class="darkblue-col text-center" style="height: 660px;">
<img src="/upload/models/trade-in/pg_premium_light_big.png" />
<hr / style="border-top: 1px solid #dddddd">
<ul>
	<li>Все автомобили, включая иностранные бренды, производимые на территории РФ*</li>
	<li>До 5 лет</li>
	<li>До 125 000 км</li>
	<li>Официально ввезённые автомобили</li>
	<li>Все ТО пройдены своевременно</li>
	<li>100 точек контроля пройдены</li>
	<li>Без аварийной истории</li>
	<li>Весь необходимый ремонт должен быть выполнен до демонстрации автомобиля</li>
	<li>1 год Поддержки на дорогах или 25 000 км</li>
	<li>1 год Гарантии или 25 000 км</li>
</ul>
</div>
</div>

<div class="col-md-4">
<div class="darkblue-col text-center" style="height: 660px;">
<img src="/upload/models/trade-in/pg_comfort_light_big.png" />
<hr / style="border-top: 1px solid #dddddd">
<ul>
	<li>Все автомобили, включая иностранные бренды, производимые на территории РФ*</li>
	<li>До 8 лет</li>
	<li>До 160 000 км</li>
	<li>Официально ввезённые автомобили</li>
	<li>100 точек контроля пройдены</li>
	<li>Без серъёзной аварийной истории</li>
	<li>Весь необходимый ремонт должен быть выполнен до демонстрации автомобиля</li>
	<li>6 мес. Поддержки на дорогах или 12 500 км</li>
	<li>6 мес. Гарантии или 12 500 км</li>
</ul>
</div>
</div>

<div class="col-md-4">
<div class="darkblue-col text-center" style="height: 660px;">
<img src="/upload/models/trade-in/pg_budget_light_big.png" />
<hr / style="border-top: 1px solid #dddddd">
<ul>
	<li>Все автомобили, включая иностранные бренды, производимые на территории РФ</li>
	<li>До 8 лет</li>
	<li>Более 160 000 км</li>
	<li>Официально ввезённые автомобили</li>
	<li>100 точек контроля пройдены</li>
	<li>Без серъёзной аварийной истории</li>
	<li>Поддержка на дорогах 12/24 мес.<span id="bx-cursor-node"> </span></li>
</ul>
</div>
</div>

</div>
<br />
<small>
<p><strong>*СПИСОК ИСКЛЮЧЕННЫХ МАРОК И МОДЕЛЕЙ для «ПРЕМИУМ» и «КОМФОРТ»</strong></p>
<p><strong>МАРКИ</strong>: Aston Martin, Bentley, Bristol, Bugatti, Caterham, De Tomaso, Ferrari, Ford Cosworth, Kit Cars, Lamborghini, Porsche, LCC Rocket, Lotus, Marcos, Maserati, McLaren F1, Noble M10/M12, Porsche, Rolls Royce, TVR, Venturi, Westfield, AC, Morgan, Hummer, Aro.&nbsp;</p>
<p><strong>МОДЕЛИ</strong>:&nbsp;BMW M/Z-серии, Jaguar «R/R-S серии», Jaguar XJ/XK/XF 5.0, Mercedes-Benz C/CL/CLS/E/S/SL/SLS/SLK /ML/G/GL 63/65/55 AMG, Daimler (12-ти цил.), Land Rover RR/RRS Supercharged/V8, Infiniti FX50S, VW Golf, GTI/R, Cadillac CTC-V, Lancia Delta Integrale/8.32, Mitsubishi 3000GT/Evo, Nissan 300ZX/Skyline/GTR, Subaru SVX/Impreza WRX/Sti/BRZ, Toyota GT86, Opel GTC/OPC, Alpina, Ford Focus ST, Mazda MX-5, модели с двигателем V8/V10.&nbsp;</p>
<p><strong>ПРОЧИЕ МАРКИ</strong>:&nbsp;ВАЗ, Niva, ГАЗ, УАЗ, Иж, Тагаз, ЗИЛ, Газель, Aro, Luxgen, Iran Khodro.&nbsp;</p>
<p><strong>ПРОЧИЕ АВТОМОБИЛИ</strong>:&nbsp;с гибридным типом топлива двигателей, бронированные.</p>
</small>

</div>
<div class="container py-5">
	<div class="row">
		<div class="col-md-12">
			<h1>Записаться на трейд-ин</h1>
		</div>
		<div class="col-md-12">
			 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"form.inline",
	Array(
		"CACHE_TIME" => "0",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "N",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "N",
		"VARIABLE_ALIASES" => Array("RESULT_ID"=>"RESULT_ID","WEB_FORM_ID"=>"WEB_FORM_ID"),
		"WEB_FORM_ID" => "7"
	)
);?>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>