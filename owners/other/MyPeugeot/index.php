<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мобильное Приложение My Peugeot");
?>

<div class="container banner">
    <div class="row pb-3 "> 
	<img src="/upload/peugeot-newsletter.159442.32.jpg" style="width: 100%;">
	<div class="infos panel grey" style="">
	<p>Мобильное приложение MY PEUGEOT  -  гид по Вашему автомобилю в Вашем смартфоне. Данные о Ваших поездках, напоминания о сроке технического обслуживания, онлайн-запись на сервис, оперативная связь с дилером  и другая полезная информация в один клик. </p>
	</div>
	</div>

<div class="row pb-3 text-center mb-3"> 
	<div class="col-sm-2 col-md-2"> </div>
	<div class="col-sm-4 col-md-4 mt-2">
		<a href="https://apps.apple.com/ru/app/id1021587274" class="but but-blue mr-2 d-block" target="_blank">СКАЧАТЬ MY PEUGEOT ДЛЯ IOS</a>
		
	</div>	
	<div class="col-sm-4 col-md-4 mt-2">
	
		<a href="https://play.google.com/store/apps/details?id=com.psa.mym.mypeugeot" class="but but-blue mr-2 d-block" target="_blank">СКАЧАТЬ MY PEUGEOT ДЛЯ ANDROID</a>
	</div>	
	<div class="col-sm-2 col-md-2"> </div>
</div>	

	 <div class="row pb-3 "> 
	 <div class="col-sm-12 col-md-12 mb-3 mt-3">
		<h2 class="text-center">ОТКРОЙТЕ ДЛЯ СЕБЯ ВСЕ ПРЕИМУЩЕСТВА MY PEUGEOT</h2>
	 </div>		
	  <div class="col-sm-4 col-md-4 mb-3 mt-3">
		<img src="/upload/my-peugeot-iban-static-630-291-3.21.jpg" style="width: 100%;">
	  </div>
	  
	  <div class="col-sm-8 col-md-8 mb-3 mt-3">
		<h3>Экономьте время и деньги </h3>
		<p>С помощью функции определения геолокации отмечайте последнее место парковки, и Вы всегда сможете найти оптимальный путь к своему автомобилю, нажав на кнопку «Проложить маршрут».</p>
		<p>Вы сможете вести статистику в разделе «Мои поездки», отслеживать расход топлива, а также рассчитать стоимость Ваших поездок.</p>
		<p>Теперь не придется искать информацию о сроках технического обслуживания в сервисной книжке! Теперь все это Вы сможете посмотреть в Вашем смартфоне в приложении My Peugeot.</p>
		<p>Записаться на сервис к Вашему дилеру или рассчитать стоимость обслуживания? Также теперь можно через приложение MyPeugeot!</p>
		<p>Приложение MY PEUGEOT совместимо со смартфонами с IOS (версия 6 и выше) и Android (версия 2.3. и выше).</p>
	  </div>
	   <div class="col-sm-4 col-md-4 mb-3 mt-3">
			<p><a href="https://apps.apple.com/ru/app/id1021587274" class="but but-blue mr-2 d-inline-block" target="_blank">MY PEUGEOT ДЛЯ IOS</a></p>
			<p><a href="https://play.google.com/store/apps/details?id=com.psa.mym.mypeugeot" class="but but-blue mr-2 d-inline-block" target="_blank">MY PEUGEOT ДЛЯ ANDROID</a></p>
	   </div>
	   <div class="col-sm-8 col-md-8 mb-3 mt-3">
			
	   </div>
	   
	   <div class="col-sm-8 col-md-8 mb-3 mt-3">
			<p>При первом входе в приложение MY PEUGEOT на свой смартфон введите VIN Вашего PEUGEOT. Подключите смартфон&nbsp; к автомобилю через Bluetooth, чтобы приложение могло получать информацию о Вашем Peugeot.*&nbsp;</p>
			<p>По завершению пути в приложение будет передана информация о поездке: расход топлива, километраж, стоимость поездки (при условии указания цены за топливо), а также уведомления систем при необходимости.*</p>
			<p><em>*доступно для автомобилей, оборудованных сенсорным экраном&nbsp; и GPS. </em></p>
			<p><em>Приложение использует Интернет-соединение, подробнее о тарифах на интернет-трафик узнавайте у Вашего оператора связи.</em></p>
			<p><a href="https://apps.apple.com/ru/app/id1021587274" class="but but-blue mr-2 mt-2 d-inline-block" target="_blank">MY PEUGEOT ДЛЯ IOS</a>
			   <a href="https://play.google.com/store/apps/details?id=com.psa.mym.mypeugeot" class="but but-blue mr-2 mt-2 d-inline-block" target="_blank">MY PEUGEOT ДЛЯ ANDROID</a></p>
	   </div>
	   <div class="col-sm-4 col-md-4 mb-3 mt-3">
			<img src="/upload/my-peugeot-iban-static-630-291-3.21.jpg" style="width: 100%;">
	   </div>
	</div>
	</div>

</div>	
</div>	




<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>