<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Руководства по эксплуатации");
?>

<div class="container banner">
    <div class="row pb-3 "> 
		<div class="col-sm-12 col-md-12 pt-3 pb-3 text-center"> 
			<h2>ВСЕ, ЧТО НЕОБХОДИМО ЗНАТЬ О:</h2>		
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/amort.pdf" target="_blank"><img src="/upload/amort.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">АМОРТИЗАТОРЫ</p>	
			<p><a href="/upload/pdf/expert-pdf/amort.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a></p>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/smaz-m.pdf" target="_blank"><img src="/upload/smaz-m.jpg " alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">СМАЗОЧНЫЕ МАТЕРИАЛЫ</p>	
			<p><a href="/upload/pdf/expert-pdf/smaz-m.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a></p>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/tech-obs.pdf" target="_blank"><img src="/upload/tech-obs.jpg " alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ТЕХНИЧЕСКОЕ ОБСЛУЖИВАНИЕ</p>	
			<p><a href="/upload/pdf/expert-pdf/tech-obs.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a></p>
		</div>
		
			<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/tormoz-s.pdf" target="_blank"><img src="/upload/tormoz-s.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ТОРМОЗНАЯ СИСТЕМА</p>	
			<a href="/upload/pdf/expert-pdf/tormoz-s.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/gamma-orig.pdf" target="_blank"><img src="/upload/gamma-orig.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ГАММА ОРИГИНАЛЬНЫХ ЗАПАСНЫХ ЧАСТЕЙ PEUGEOT</p>	
			<a href="/upload/pdf/expert-pdf/gamma-orig.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/osvehenie.pdf" target="_blank"><img src="/upload/osvehenie.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ОСВЕЩЕНИЕ</p>	
			<a href="/upload/pdf/expert-pdf/osvehenie.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/vipysk-system.pdf" target="_blank"><img src="/upload/vipysk-system.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ВЫПУСКНАЯ СИСТЕМА И САЖЕВЫЙ ФИЛЬТР</p>	
			<a href="/upload/pdf/expert-pdf/vipysk-system.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/lob-glass.pdf" target="_blank"><img src="/upload/lob-glass.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ЛОБОВОЕ СТЕКЛО</p>	
			<a href="/upload/pdf/expert-pdf/lob-glass.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/shini.pdf" target="_blank"><img src="/upload/shini.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ШИНЫ</p>	
			<a href="/upload/pdf/expert-pdf/shini.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/winter-shini.pdf" target="_blank"><img src="/upload/winter-shini.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ЗИМНИЕ ШИНЫ</p>	
			<a href="/upload/pdf/expert-pdf/winter-shini.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<!-- <div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/" target="_blank"><img src="/upload/shetki.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">ЩЕТКИ СТЕКЛООЧИСТИТЕЛЯ</p>	
			<a href="/" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div> -->
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/conditioner.pdf" target="_blank"><img src="/upload/conditioner.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">КОНДИЦИОНЕР И ФИЛЬТР САЛОНА</p>	
			<a href="/upload/pdf/expert-pdf/conditioner.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
			<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/grm.pdf" target="_blank"><img src="/upload/grm.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">КОМПЛЕКТ ГРМ</p>	
			<a href="/upload/pdf/expert-pdf/grm.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/expert-pdf/acum-battare.pdf" target="_blank"><img src="/upload/acum-battare.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">АККУМУЛЯТОРНАЯ БАТАРЕЯ</p>	
			<a href="/upload/pdf/expert-pdf/acum-battare.pdf" class="but but-blue mb-2 " target="_blank">СКАЧАТЬ БРОШЮРУ</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<!-- <a href="/upload/pdf/308.pdf" target="_blank"><img src="/upload/shini.jpg" alt="" class="img-fluid"> </a>
            <p class="pt-3 pb-3">КОНДИЦИОНЕР И ФИЛЬТР САЛОНА</p>	
			<a href="/upload/pdf/308.pdf" class="but but-blue mb-2 ">СКАЧАТЬ БРОШЮРУ</a> -->
		</div>
		
	</div>
	</div>






<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>