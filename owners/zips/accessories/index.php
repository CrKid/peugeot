<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оригинальные аксессуары Peugeot");
?>

	<div class="container">
    <div class="row pt-5 pb-3"> 
     
        <div class="col-sm-4 col-md-4 p-0"> <img src="/upload/9751-3acc83.82014.21.jpg" alt="" style="width: 100%;"> </div>
        <div class="col-sm-8 col-md-8 ">
            <h2 style="text-align: center;">Каталоги аксессуаров</h2> 
            <p>Вы стали владельцем автомобиля <span lang="EN-US">Peugeot</span>, сочетающего в себе выразительную внешность, прекрасную управляемость и высокий уровень безопасности. Специально разработанные аксессуары сделают Ваш автомобиль более стильным, комфортным и заметным. Для удобства выбора мы специально создали каталоги, где Вы можете подобрать аксессуары, которые подчеркнут неповторимый облик автомобиля и сделают его максимально приспособленным к Вашему образу жизни.</p>		
        </div>
		
		 </div>
  
	
	   <div class="row pt-3 pb-3"> 
		<div class="col-sm-12 col-md-12 ">
            <h3 style="text-align: center;">ВЫБЕРИТЕ КАТАЛОГ УНИКАЛЬНЫХ АКСЕССУАРОВ ДЛЯ СВОЕГО PEUGEOT</h3> 		
		</div>
		</div>
		
		<div class="row pt-3 pb-3"> 
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/308.pdf" target="_blank"><img src="/upload/308-accessories-image.114383.114383.6.256987.6.jpg" alt=""  class="img-fluid"> </a>
            <p class="pt-3 pb-3">PEUGEOT 308</p>	
			<a href="/upload/pdf/308.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT 308</a>
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
           <a href="/upload/pdf/2008.pdf" target="_blank"><img src="/upload/2008-suv-accessories-image.114395.114395.6.256995.6.jpg" alt="" class="img-fluid"></a>
			 <p class="pt-3 pb-3">НОВЫЙ PEUGEOT 2008</p>
			 <a href="/upload/pdf/2008.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT 2008</a>
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
            <a href="/upload/pdf/3008.pdf" target="_blank"><img src="/upload/3008-suv-accessories-image.148078.148078.6.256993.6.jpg" alt="" class="img-fluid"></a>		
			 <p class="pt-3 pb-3">НОВЫЙ PEUGEOT 3008</p>
			 <a href="/upload/pdf/3008.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT 3008</a>
		</div>
		
		<div class="col-sm-4 col-md-4 p-1 pb-5">
			 <a href="/upload/pdf/expert.pdf" target="_blank"><img src="/upload/expert-accessories-image.114401.114401.6.256991.6.jpg " alt=""  class="img-fluid"></a>
            <p class="pt-3 pb-3">PEUGEOT EXPERT</p>	
			<a href="/upload/pdf/expert.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT EXPERT</a>
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
            <a href="/upload/pdf/traveller.pdf" target="_blank"> <img src="/upload/traveller-accessories-image.114407.114407.114407.6.256989.6.jpg" alt="" class="img-fluid">	</a>
			 <p class="pt-3 pb-3">PEUGEOT TRAVELLER</p>
			 <a href="/upload/pdf/traveller.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT TRAVELLER</a>
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
             <a href="/upload/pdf/408.pdf" target="_blank"><img src="/upload/acs408-1.6.jpg" alt="" class="img-fluid"></a>		
			 <p class="pt-3 pb-3">НОВЫЙ PEUGEOT 408</p>
			 <a href="/upload/pdf/408.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА НОВЫЙ PEUGEOT 408</a>
		</div>
		
			<div class="col-sm-4 col-md-4 p-1 pb-5">
			<a href="/upload/pdf/5008.pdf" target="_blank"><img src="/upload/-2018-01-31-13.11.01.375665.6.png" alt=""  class="img-fluid"></a>
            <p class="pt-3 pb-3">НОВЫЙ PEUGEOT 5008</p>	
			<a href="/upload/pdf/5008.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT 5008</a>
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
            <a href="/upload/pdf/boxer.pdf" target="_blank"><img src="/upload/boxer-acs.407191.6.jpg" alt="" class="img-fluid">	</a>
			 <p class="pt-3 pb-3">НОВЫЙ PEUGEOT BOXER</p>
			 <a href="/upload/pdf/boxer.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT BOXER</a>
		</div>
		<div class="col-sm-4 col-md-4 p-1 pb-5">
           <a href="/upload/pdf/partner.pdf" target="_blank"> <img src="/upload/tepee-acs.407193.6.jpg" alt="" class="img-fluid"></a>
			 <p class="pt-3 pb-3">PEUGEOT PARTNER TEPEE</p>
			 <a href="/upload/pdf/partner.pdf" class="but but-blue mb-2 d-inline-block p-2" target="_blank" style="line-height: 40px;">АКСЕССУАРЫ НА PEUGEOT PARTNER TEPEE</a>
		</div>
		</div>
		
		<div class="row pt-3 pb-3"> 
		<div class="col-sm-12 col-md-12 ">
            <h3 style="text-align: center;">ВИДЕООБЗОРЫ УНИКАЛЬНЫХ АКСЕССУАРОВ</h3> 		
		</div>
		</div>
		
		<div class="row pt-3 pb-3"> 
		<div class="col-sm-6 col-md-6 p-1 pb-5">
			 <a href="/owners/zips/accessories/video-3008/" target="_blank"><img src="/upload/video-3008.19.jpg" alt=""  class="img-fluid"></a>
            <p class="pt-3 pb-3">PEUGEOT 3008</p>	
			<a href="/owners/zips/accessories/video-3008/" class="but but-blue mb-2 " target="_blank">СМОТРЕТЬ</a>
		</div>
		<div class="col-sm-6 col-md-6 p-1 pb-5">
            <a href="/owners/zips/accessories/video-2008/" target="_blank"> <img src="/upload/video-2008.19.jpg" alt="" class="img-fluid">	</a>
			 <p class="pt-3 pb-3">НОВЫЙ PEUGEOT 2008</p>
			 <a href="/owners/zips/accessories/video-2008/" class="but but-blue mb-2 " target="_blank">СМОТРЕТЬ</a>
		</div>
		<div class="col-sm-6 col-md-6 p-1 pb-5">
            <a href="/owners/zips/accessories/video-308/" target="_blank"> <img src="/upload/308-video.326681.19.jpg" alt="" class="img-fluid">	</a>
			 <p class="pt-3 pb-3">НОВЫЙ PEUGEOT 308</p>
			 <a href="/owners/zips/accessories/video-308/" class="but but-blue mb-2 " target="_blank">СМОТРЕТЬ</a>
		</div>
		</div>

</div>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>